## PATH of cross-compiler toolchain files

####################  Do not touch below if you don't know #####################
set(CMAKE_SYSTEM_NAME        Linux)
set(CMAKE_SYSTEM_VERSION     1)
set(CMAKE_SYSTEM_PROCESSOR   arm)
set(CMAKE_TOOLCHAIN_PREFIX   arm-linux-gnueabihf)

set(TOOLCHAIN_ROOT_PATH      /usr/${CMAKE_TOOLCHAIN_PREFIX})

set(CMAKE_C_COMPILER         ${CMAKE_TOOLCHAIN_PREFIX}-gcc)
set(CMAKE_CXX_COMPILER       ${CMAKE_TOOLCHAIN_PREFIX}-g++)
set(CMAKE_FIND_ROOT_PATH     ${TOOLCHAIN_ROOT_PATH})
set(CMAKE_C_FLAGS            "-march=armv7-a -mtune=cortex-a8 -mcpu=cortex-a8 -mfpu=vfpv3-d16 -mfloat-abi=hard -mthumb" CACHE STRING "c flags")
#set(CMAKE_C_FLAGS            "-march=armv7-a -mtune=cortex-a8 -mcpu=cortex-a8 -mfloat-abi=softfp -mfpu=neon " CACHE STRING "c flags")
set(CMAKE_CXX_FLAGS          "${CMAKE_C_FLAGS} -static-libgcc -static-libstdc++" CACHE STRING "c++ flags")
