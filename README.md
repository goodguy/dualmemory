### 커널코드 클론 및 빌드 ###

	cd ~/
	mkdir bbb && cd bbb
	sudo apt-get install git libncurses5-dev lzop cmake
	git clone https://github.com/goodguykor/linux.git -b 3.8.13-bone-xenomai beagle-kernel --depth 20
	[http://elinux.org/EBC_Xenomai] 참고하기 
	wget https://xenomai.org/downloads/xenomai/stable/xenomai-2.6.4.tar.bz2
	tar xjf ./xenomai-2.6.4.tar.bz2
	cd xenomai-2.6.4/scripts
	./prepare-kernel.sh --arch=arm --linux=../../beagle-kernel/
	cd ~/bbb/beagle-kernel
	[Ubuntu 14.04] 
		sudo apt-get install -y gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf
	[Ubuntu 16.04] 
		sudo apt-get install gcc-4.9-arm-linux-gnueabihf g++-4.9-arm-linux-gnueabihf
		sudo update-alternatives --install /usr/bin/arm-linux-gnueabihf-gcc "gcc-arm-linux" "/usr/bin/arm-linux-gnueabihf-gcc-4.9" 1
		sudo update-alternatives --install /usr/bin/arm-linux-gnueabihf-g++ "g++-arm-linux" "/usr/bin/arm-linux-gnueabihf-g++-4.9" 1

	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- distclean
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bb.org_defconfig
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- menuconfig
	[제노마이 관련 kernel menuconfig 수행]
		(Under CPU Power Management ---	 CPU Frequency scaling, disable [ ] CPU Frequency scaling)  
		(Under Real-time sub-system ---	 Drivers ---	 Testing drivers, enable everything)
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -j4 bzImage
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -j4 modules
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -j4 firmware

### 커널 이미지 및 모듈 설치 ###

	[SD카드를 컴퓨터에 마운트] 
	export SD_PATH=**마운트된폴더**
	cd ~/bbb/beagle-kernel 
	sudo cp arch/arm/boot/zImage $SD_PATH/boot/vmlinuz-3.8.13
	sudo -E "PATH=$PATH" make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=$SD_PATH modules_install
	sudo -E "PATH=$PATH" make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=$SD_PATH firmware_install
	sudo umount $SD_PATH

### Xenomai 2.6 크로스 컴파일 및 시스템 설치하기 ###

	cd ~/bbb/
	mkdir xenomai/
	cd xenomai-2.6.4/
	./configure CFLAGS="-march=armv7-a -mfpu=vfpv3-d16 -mfloat-abi=hard" LDFLAGS="-march=armv7-a -mfpu=vfpv3-d16 -mfloat-abi=hard"  --build=x86_64-linux-gnu --target=arm-linux-gnueabihf --host=arm-linux-gnueabihf --prefix=`pwd`/../xenomai/
	make install -j4

### DPRAM 모듈 및 테스트 프로그램 크로스 컴파일하기 ###

	cd ~/bbb/
	mkdir library
	git clone https://bitbucket.org/goodguy/dualmemory.git
	cd dualmemory/
	mkdir build/ && cd build/
	cmake -DCMAKE_TOOLCHAIN_FILE=../bbb.cmake -DCMAKE_INSTALL_PREFIX=${HOME}/bbb/library/ ..
	make -j3


### DPRAM 모듈 및 테스트 프로그램 업로드하기 ###

	cd ~/bbb/dualmemory/build/
	[BBB를 컴퓨터에 연결]
	make copy                  [pw: 1]
	ssh root@192.168.7.2       [pw: 1]
		ls -alh ./
		[옮겨진것을 확인 할 수 있다]

### DPRAM 모듈 및 테스트 프로그램 구동하기 ###

	ssh root@192.168.7.2       [pw: 1]
	./initial.sh [BBB 부팅이후에만 수행]
	insmod dpram.ko
	./rtdm_test_write
	./rtdm_test_read

### DPRAM myCortex 펌웨어 크로스 컴파일하기 ###

	sudo apt-get install -y gcc-arm-none-eabi openocd
	cd ~/bbb/dualmemory/mycortex/
	mkdir build/ && cd build/
	cmake -DCMAKE_TOOLCHAIN_FILE=../mycortex.cmake ..
	make -j3 

 
### DPRAM myCortex 펌웨어 업로드하기 ###

	[ST-Link/v2를 myCortex에 부착 후 컴퓨터에 연결]
	cd ~/bbb/dualmemory/mycortex/build/
	make flash_test_stm
		실패할 경우에는 ST-Link/v2를 컴퓨터에 재연결한다.


### Boost library cross-compile 하기 ###
	
	Downloads boost library ver. 1.59.0 [https://sourceforge.net/projects/boost/files/boost/1.59.0/boost_1_59_0.tar.bz2/download]
	mkdir ~/bbb/build_library/ && cd ~/bbb/build_library
	tar xjf boost_1_59_0.tar.bz2 
	cd boost_1_59_0
	./bootstrap.sh
	vi project-config.jam
		아래를 변경한다
		[변경전] using gcc ;
		[변경후] using gcc : arm : arm-linux-gnueabihf-g++ : <compileflags>"-march=armv7-a -mtune=cortex-a8 -mcpu=cortex-a8 -mfpu=vfpv3-d16 -mfloat-abi=hard -mthumb -static-libstdc++" ;
	./b2   toolset=gcc-arm variant=release link=shared threading=multi runtime-link=shared  --prefix=${HOME}/bbb/library --with-chrono --with-date_time --with-filesystem --with-regex --with-system --with-timer --with-thread -a install -j8

### Eigen library cross-compile 하기 ###

	cd ~/bbb/build_library/
	wget http://bitbucket.org/eigen/eigen/get/3.3.4.tar.bz2
	mkdir eigen3
	tar xjf 3.3.4.tar.bz2 -C eigen3 --strip-components=1
	cd eigen3
	mkdir build && cd build
	cmake -DCMAKE_TOOLCHAIN_FILE=../../dualmemory/bbb.cmake -DCMAKE_INSTALL_PREFIX=${HOME}/bbb/library/ ..
	make -j4 && make install -j4

### Hokuyo library cross-compile 하기 ###

	cd ~/bbb/build_library/
	git clone https://github.com/gbiggs/flexiport.git
	cd flexiport/
	mkdir build && cd build
	cmake -DCMAKE_TOOLCHAIN_FILE=../../dualmemory/bbb.cmake -DCMAKE_INSTALL_PREFIX=${HOME}/bbb/library/ -DBUILD_DOCUMENTATION=false ..
	make -j4 && make install -j4
	cd ~/bbb/build_library/
	git clone https://github.com/gbiggs/hokuyoaist.git
	cd hokuyoaist/
	mkdir build && cd build
	cmake -DCMAKE_TOOLCHAIN_FILE=../../dualmemory/bbb.cmake -DCMAKE_INSTALL_PREFIX=${HOME}/bbb/library/ -DBUILD_DOCUMENTATION=false ..
	make -j4 && make install -j4
	
     
### 제어 프로그램 컴파일 및 업로드하기 ###

	cd ~/bbb/dualmemory/build/
	cmake -DCMAKE_TOOLCHAIN_FILE=../../dualmemory/bbb.cmake -DCMAKE_INSTALL_PREFIX=${HOME}/bbb/library/  ..

	[BBB를 컴퓨터에 연결]
	make copy_controller       [pw: 1]
	ssh root@192.168.7.2       [pw: 1]
		ls -alh ./
		[옮겨진것을 확인 할 수 있다]


### 제어 프로그램 구동하기 ###

	ssh root@192.168.7.2       [pw: 1]
	./initial.sh [BBB 부팅이후에만 수행]
	insmod dpram.ko
	./controller

