#include <iostream>
#include <cmath>


/* CPU를 의도적으로 과부화시키는 프로그램*/
int main(){

    volatile unsigned int x = 3;
    volatile unsigned int y = 4;
    volatile unsigned int z = 0;

    while(1){
        z = std::sqrt(x*x + y*y);
    }
    return 0;
}
