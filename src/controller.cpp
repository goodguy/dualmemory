#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
#include <vector>
#include <Eigen/Eigen>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>        // strlen()
#include <fcntl.h>         // O_WRONLY
#include <unistd.h>        // write(), close()
#include <dpram_dev.h>
#include <time.h>
#include <sys/mman.h>
#include <rtdm/rtdm.h>
#include <native/task.h>
#include <native/timer.h>


#include <flexiport/flexiport.h>
#include <hokuyoaist/hokuyoaist.h>
#include <hokuyoaist/hokuyo_errors.h>


#include <boost/asio.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/timer/timer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>



/////////////////////////////////////////////////////////////////////////////////
////////for MWPG /////////
//#define dbg(x)	printf("%d\n", x)
#define dbg(x)	

// physical constant
#define	PI		3.14159265358979323846
#define grav	9.81

#define T_ctrl			0.005// sample time for MWPG & control
#define Tsample			0.005
#define isnan(x)	((x) != (x))
#define N_ITER_BINARY_SEARCH	1000	// binary search를 위한 최대 반복값



//#define BIANRY
//#define VSD

#define CYCLOID1
//#define CYCLOID2

double GAIN_roll=1.5;
double global_P, global_Q;

#define TOTAL_NUM_OF_STEPS	6
static double CS_list[][10] = {
	{0.6, 0.6, 0.6, 0.6, 0.05, 0.05,  0.2, -0.2, 0.0/180*PI,  0.0/180.*PI},  
  {0.6, 0.6, 0.6, 0.6, 0.05, 0.05,  0.2, -0.2, 0.0/180*PI,  0.0/180.*PI},   
  {0.6, 0.6, 0.6, 0.6, 0.05, 0.05,  0.2, -0.2, 0.0/180*PI,  0.0/180.*PI},  
  {0.6, 0.6, 0.6, 0.6, 0.05, 0.05,  0.2, -0.2, 0.0/180*PI,  0.0/180.*PI},  
  {0.6, 0.6, 0.6, 0.6, 0.05, 0.05,  0.2, -0.2, 0.0/180*PI,  0.0/180.*PI},  
  {0.6, 0.6, 0.6, 0.6, 0.05, 0.05,  0.2, -0.2, 0.0/180*PI,  0.0/180.*PI},  
};



double Le_x, Le_y, Re_x, Re_y;
double F_X, F_Y, F_Z, F_R;
int S_leg;

double cur_Loffset_rot_x=0., past_Loffset_rot_x=0.;
double cur_Loffset_rot_y=0., past_Loffset_rot_y=0.;
double cur_Loffset_tran_z=0., past_Loffset_tran_z=0.;
double cur_Roffset_rot_x=0., past_Roffset_rot_x=0.;
double cur_Roffset_rot_y=0., past_Roffset_rot_y=0.;
double cur_Roffset_tran_z=0., past_Roffset_tran_z=0.;
 
double force[8], pos_x[8], pos_y[8], zmp[2];
double force_left, force_right, force_sum;
  double Ltorque_x;
  double Ltorque_y;
  double Lforce_z;
  double Rtorque_x;
  double Rtorque_y;
  double Rforce_z;
  double L_k_rot_x, L_k_rot_y, L_k_tran_z, L_b_rot_x=0., L_b_rot_y=0., L_b_tran_z=0.;
  double L_k_rot_dx=0., L_k_rot_dy=0., L_k_tran_dz=0., L_b_rot_dx=0., L_b_rot_dy=0., L_b_tran_dz=0.;
  double R_k_rot_x, R_k_rot_y, R_k_tran_z, R_b_rot_x, R_b_rot_y, R_b_tran_z;
  double R_k_rot_dx=0., R_k_rot_dy=0., R_k_tran_dz=0., R_b_rot_dx=0., R_b_rot_dy=0., R_b_tran_dz=0.;


typedef struct _ref{
	// angle
	double hipz_q[2];
	double hipx_q[2];
	double hipy_q[2];
	double kneey_q[2];
	double ankley_q[2];
	double anklex_q[2];
	// angular velocity
	double hipz_qd[2];
	double hipx_qd[2];
	double hipy_qd[2];
	double kneey_qd[2];
	double ankley_qd[2];
	double anklex_qd[2];
} REF;


typedef struct _mwpg{
	//double WS[2][2];	// [XX/YY][x/T_cxd]

	double PELVIS_LENGTH;
	double THIGH_LENGTH, SHIN_LENGTH;
	double Z_c;
	double HIP_CENTER2CM[3];	// hip 중심에서 무게 중심까지의 x 좌표
	double SOLE2ANKLE;
	double gravity;
	double T_c;
	double T_samples;
	double P_MAX, P_MIN, Q_MAX, Q_MIN;
	double CS_STOP[10];
	double FIW, FOW, FFL, FBL;
	double Rc;
	double T_ds_start, T_ds_stop;
	double T_SS_MAX;
	int DS_START, SS, DS, DS_STOP, STOP;
	int LEFT, RIGHT;
	int TRUE, FALSE;
	double NEAR_ZERO;
	int PLUS_TOLERANCE;
	int MINUS_TOLERANCE;
	double F_XY_ACC_MAX, F_Z_ACC_MAX;
} MWPG;

typedef struct _wp{
	double WS[4];	// WS[0] = x, WS[1] = vTc, WS[2] = y, WS[3] = ywTc
	double CM_R[2];	// CM_R[0] = q, CM_R[1] = dq
	double FS[6];	// FS[0] = x, FS[1] = v, FS[2] = y, FS[3] = w, FS[4] = z, FS[5] = u
	double F_R[2];	// F_R[0] = q, F_R[1] = dq where q is z-azis rotation
	double CC[2];	// CC[0] = q_cycloid, CC[1] = dq_cycloid
	int sup_leg;	// supporting leg
	int state;
	double T_ds_remain;
} WP;


static MWPG mwpg;


//MWPG init_robot();
int convert_state_wrt_global(double WS_local[4], double CM_R_local[2], double FS_local[4], double F_R_local[2], double Offset[3],
	double WS_global[4], double CM_R_global[2], double FS_global[4], double F_R_global[2]);
int compute_reference_CM_foot_motion(double CS[10], double CS_old[10], 
	WP *wp_k, int stop_flag, WP *wp_kp1, double CS_mod[10]);
int compute_desired_WS(double CS[10], int supporting_leg, double WS_d[4]);
int walking_pattern(REF *ref);
int ready_position(double q_init[12], REF *ref);
REF forward_kinematics(WP *wp);
int init_mwpg();

int mat2_2_transpose(double A[2][2], double B[2][2]);
int mat2_2_2_mult(double A[2][2], double B[2][2], double C[2][2]);
int mat2_2_1_mult(double A[2][2], double b[2], double c[2]);
int mat2_2_scale(double A[2][2], double b, double C[2][2]);
int mat2_2_add(double A[2][2], double B[2][2], double C[2][2]);
int mat2_1_add(double a[2], double b[2], double c[2]);
int mat2_1_copy(double a[2], double b[2]);
double mat4_4_determinant(double A[4][4]);
int mat4_4_inv_mult_mat4_1(double A[4][4], double b[4], double c[4]);
int mat2_2_to_kronecker_product(double A[2][2], double B[2][2], double C[4][4]);
int mat4_4_4_mult(double A[4][4], double B[4][4], double C[4][4]);
int mat4_4_sub(double A[4][4], double B[4][4], double C[4][4]);
int mat4_4_1_mult(double A[4][4], double b[4], double c[4]);
int mat4_1_add(double a[4], double b[4], double c[4]);
int mat6_1_copy(double a[6], double b[6]);
int mat10_1_copy(double a[10], double b[10]);
int mat10_1_add(double a[10], double b[10], double c[10]);
int mat10_1_scale(double a[10], double b, double c[10]);
int mat2_1_scale(double a[2], double b, double c[2]);
int mat4_1_copy(double a[4], double b[4]);
int mat10_10_diag(double a[10], double B[10][10]);
int mat4_4_diag(double a[4], double B[4][4]);
int mat4_1_scale(double a[4], double b, double c[4]);
int mat3_1_copy(double a[3], double b[3]);
int mat3_1_scale(double a[3], double b, double c[3]);
int transformation_inv(double A[4][4], double B[4][4]);
int cross_product(double a[3], double b[3], double c[3]);


int mat2_2_transpose(double A[2][2], double B[2][2]){
	int i, j;

	for(i=0; i<2; i++) {
		for(j=0; j<2; j++) {
			B[i][j] = A[j][i];
		}
	}

	return 0;
}

int mat2_2_2_mult(double A[2][2], double B[2][2], double C[2][2]) {
	int i, j, k;

	for(i=0; i<2; i++) {
		for(j=0; j<2; j++) {
			C[i][j] = 0;
			for(k=0; k<2; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}

	return 0;
}

int mat2_2_1_mult(double A[2][2], double b[2], double c[2]) {
	int i, j;

	for(i=0; i<2; i++) {
		c[i] = 0;
		for(j=0; j<2; j++) {
			c[i] += A[i][j] * b[j];
		}
	}

	return 0;
}

int mat2_2_scale(double A[2][2], double b, double C[2][2]) {
	int i, j;

	for(i=0; i<2; i++) {
		for(j=0; j<2; j++) {
			C[i][j] = A[i][j] * b;
		}
	}

	return 0;
}

int mat2_2_add(double A[2][2], double B[2][2], double C[2][2]) {
	int i, j;

	for(i=0; i<2; i++) {
		for(j=0; j<2; j++) {
			C[i][j] = A[i][j] + B[i][j];
		}
	}

	return 0;
}

int mat2_1_add(double a[2], double b[2], double c[2]) {
	int i;

	for(i=0; i<2; i++) {
		c[i] = a[i] + b[i];
	}

	return 0;
}

int mat2_1_copy(double a[2], double b[2]) {
	int i;

	for(i=0; i<2; i++) {
		b[i] = a[i];
	}

	return 0;
}

double mat4_4_determinant(double A[4][4]) {
	double A1_1, A1_2, A1_3, A1_4;
	double A2_1, A2_2, A2_3, A2_4;
	double A3_1, A3_2, A3_3, A3_4;
	double A4_1, A4_2, A4_3, A4_4;
	double det;

	A1_1 = A[0][0]; A1_2 = A[0][1]; A1_3 = A[0][2]; A1_4 = A[0][3];
	A2_1 = A[1][0]; A2_2 = A[1][1]; A2_3 = A[1][2]; A2_4 = A[1][3];
	A3_1 = A[2][0]; A3_2 = A[2][1]; A3_3 = A[2][2]; A3_4 = A[2][3];
	A4_1 = A[3][0]; A4_2 = A[3][1]; A4_3 = A[3][2]; A4_4 = A[3][3];

	det = (A1_1*A2_2*A3_3*A4_4 - A1_1*A2_2*A3_4*A4_3 - A1_1*A2_3*A3_2*A4_4 + A1_1*A2_3*A3_4*A4_2 + A1_1*A2_4*A3_2*A4_3 
		- A1_1*A2_4*A3_3*A4_2 - A1_2*A2_1*A3_3*A4_4 + A1_2*A2_1*A3_4*A4_3 + A1_2*A2_3*A3_1*A4_4 - A1_2*A2_3*A3_4*A4_1 
		- A1_2*A2_4*A3_1*A4_3 + A1_2*A2_4*A3_3*A4_1 + A1_3*A2_1*A3_2*A4_4 - A1_3*A2_1*A3_4*A4_2 - A1_3*A2_2*A3_1*A4_4 
		+ A1_3*A2_2*A3_4*A4_1 + A1_3*A2_4*A3_1*A4_2 - A1_3*A2_4*A3_2*A4_1 - A1_4*A2_1*A3_2*A4_3 + A1_4*A2_1*A3_3*A4_2 
		+ A1_4*A2_2*A3_1*A4_3 - A1_4*A2_2*A3_3*A4_1 - A1_4*A2_3*A3_1*A4_2 + A1_4*A2_3*A3_2*A4_1);

	return det;
}

int mat4_4_inv_mult_mat4_1(double A[4][4], double b[4], double c[4]) {
	double A1_1, A1_2, A1_3, A1_4;
	double A2_1, A2_2, A2_3, A2_4;
	double A3_1, A3_2, A3_3, A3_4;
	double A4_1, A4_2, A4_3, A4_4;
	double b1, b2, b3, b4;
	double det;

	A1_1 = A[0][0]; A1_2 = A[0][1]; A1_3 = A[0][2]; A1_4 = A[0][3];
	A2_1 = A[1][0]; A2_2 = A[1][1]; A2_3 = A[1][2]; A2_4 = A[1][3];
	A3_1 = A[2][0]; A3_2 = A[2][1]; A3_3 = A[2][2]; A3_4 = A[2][3];
	A4_1 = A[3][0]; A4_2 = A[3][1]; A4_3 = A[3][2]; A4_4 = A[3][3];

	b1 = b[0]; b2 = b[1]; b3 = b[2]; b4 = b[3];

	det = mat4_4_determinant(A);

	c[0] =-(A1_2*A2_3*A3_4*b4 - A1_2*A2_4*A3_3*b4 - A1_3*A2_2*A3_4*b4 + A1_3*A2_4*A3_2*b4 + A1_4*A2_2*A3_3*b4 
		- A1_4*A2_3*A3_2*b4 - A1_2*A2_3*A4_4*b3 + A1_2*A2_4*A4_3*b3 + A1_3*A2_2*A4_4*b3 - A1_3*A2_4*A4_2*b3 
		- A1_4*A2_2*A4_3*b3 + A1_4*A2_3*A4_2*b3 + A1_2*A3_3*A4_4*b2 - A1_2*A3_4*A4_3*b2 - A1_3*A3_2*A4_4*b2 
		+ A1_3*A3_4*A4_2*b2 + A1_4*A3_2*A4_3*b2 - A1_4*A3_3*A4_2*b2 - A2_2*A3_3*A4_4*b1 + A2_2*A3_4*A4_3*b1 
		+ A2_3*A3_2*A4_4*b1 - A2_3*A3_4*A4_2*b1 - A2_4*A3_2*A4_3*b1 + A2_4*A3_3*A4_2*b1)/det;
	c[1] = (A1_1*A2_3*A3_4*b4 - A1_1*A2_4*A3_3*b4 - A1_3*A2_1*A3_4*b4 + A1_3*A2_4*A3_1*b4 + A1_4*A2_1*A3_3*b4 
		- A1_4*A2_3*A3_1*b4 - A1_1*A2_3*A4_4*b3 + A1_1*A2_4*A4_3*b3 + A1_3*A2_1*A4_4*b3 - A1_3*A2_4*A4_1*b3 
		- A1_4*A2_1*A4_3*b3 + A1_4*A2_3*A4_1*b3 + A1_1*A3_3*A4_4*b2 - A1_1*A3_4*A4_3*b2 - A1_3*A3_1*A4_4*b2 
		+ A1_3*A3_4*A4_1*b2 + A1_4*A3_1*A4_3*b2 - A1_4*A3_3*A4_1*b2 - A2_1*A3_3*A4_4*b1 + A2_1*A3_4*A4_3*b1 
		+ A2_3*A3_1*A4_4*b1 - A2_3*A3_4*A4_1*b1 - A2_4*A3_1*A4_3*b1 + A2_4*A3_3*A4_1*b1)/det;
	c[2] = -(A1_1*A2_2*A3_4*b4 - A1_1*A2_4*A3_2*b4 - A1_2*A2_1*A3_4*b4 + A1_2*A2_4*A3_1*b4 + A1_4*A2_1*A3_2*b4 
		- A1_4*A2_2*A3_1*b4 - A1_1*A2_2*A4_4*b3 + A1_1*A2_4*A4_2*b3 + A1_2*A2_1*A4_4*b3 - A1_2*A2_4*A4_1*b3 
		- A1_4*A2_1*A4_2*b3 + A1_4*A2_2*A4_1*b3 + A1_1*A3_2*A4_4*b2 - A1_1*A3_4*A4_2*b2 - A1_2*A3_1*A4_4*b2 
		+ A1_2*A3_4*A4_1*b2 + A1_4*A3_1*A4_2*b2 - A1_4*A3_2*A4_1*b2 - A2_1*A3_2*A4_4*b1 + A2_1*A3_4*A4_2*b1 
		+ A2_2*A3_1*A4_4*b1 - A2_2*A3_4*A4_1*b1 - A2_4*A3_1*A4_2*b1 + A2_4*A3_2*A4_1*b1)/det;
	c[3] = (A1_1*A2_2*A3_3*b4 - A1_1*A2_3*A3_2*b4 - A1_2*A2_1*A3_3*b4 + A1_2*A2_3*A3_1*b4 + A1_3*A2_1*A3_2*b4 
		- A1_3*A2_2*A3_1*b4 - A1_1*A2_2*A4_3*b3 + A1_1*A2_3*A4_2*b3 + A1_2*A2_1*A4_3*b3 - A1_2*A2_3*A4_1*b3 
		- A1_3*A2_1*A4_2*b3 + A1_3*A2_2*A4_1*b3 + A1_1*A3_2*A4_3*b2 - A1_1*A3_3*A4_2*b2 - A1_2*A3_1*A4_3*b2 
		+ A1_2*A3_3*A4_1*b2 + A1_3*A3_1*A4_2*b2 - A1_3*A3_2*A4_1*b2 - A2_1*A3_2*A4_3*b1 + A2_1*A3_3*A4_2*b1 
		+ A2_2*A3_1*A4_3*b1 - A2_2*A3_3*A4_1*b1 - A2_3*A3_1*A4_2*b1 + A2_3*A3_2*A4_1*b1)/det;
 
	return 0;
}

int mat2_2_to_kronecker_product(double A[2][2], double B[2][2], double C[4][4]) {
	int i, j, k, l;

	for(i=0; i<2; i++) {
		for(j=0; j<2; j++) {
			for(k=0; k<2; k++) {
				for(l=0; l<2; l++) {
					C[i*2 + k][j*2 + l] = A[i][j]*B[k][l];
				}
			}
		}
	}

	return 0;
}

int mat4_4_4_mult(double A[4][4], double B[4][4], double C[4][4]) {
	int i, j, k;

	for(i=0; i<4; i++) {
		for(j=0; j<4; j++) {
			C[i][j] = 0.0;
			for(k=0; k<4; k++) {
				C[i][j] += A[i][k]*B[k][j];
			}
		}
	}

	return 0;
}

int mat4_4_sub(double A[4][4], double B[4][4], double C[4][4]) {
	int i, j;

	for(i=0; i<4; i++) {
		for(j=0; j<4; j++) {
			C[i][j] = A[i][j] - B[i][j];
		}
	}

	return 0;
}

int mat4_4_1_mult(double A[4][4], double b[4], double c[4]) {
	int i, j;

	for(i=0; i<4; i++) {
		c[i] = 0.0;
		for(j=0; j<4; j++) {
			c[i] += A[i][j] *b[j];
		}
	}

	return 0;
}

int mat4_1_add(double a[4], double b[4], double c[4]) {
	int i;

	for(i=0; i<4; i++) {
		c[i] = a[i] + b[i];
	}

	return 0;
}

int mat6_1_copy(double a[6], double b[6]) {
	int i;

	for(i=0; i<6; i++) {
		b[i] = a[i];
	}

	return 0;
}

int mat10_1_copy(double a[10], double b[10]) {
	int i;

	for(i=0; i<10; i++) {
		b[i] = a[i];
	}

	return 0;
}

int mat10_1_add(double a[10], double b[10], double c[10]) {
	int i;

	for(i=0; i<10; i++) {
		c[i] = a[i] + b[i];
	}

	return 0;
}

int mat10_1_scale(double a[10], double b, double c[10]) {
	int i;

	for(i=0; i<10; i++) {
		c[i] = a[i] * b;
	}

	return 0;
}

int mat2_1_scale(double a[2], double b, double c[2]) {
	c[0] = a[0]*b;
	c[1] = a[1]*b;

	return 0;
}

int mat4_1_copy(double a[4], double b[4]) {
	b[0] = a[0];
	b[1] = a[1];
	b[2] = a[2];
	b[3] = a[3];

	return 0;
}

int mat10_10_diag(double a[10], double B[10][10]) {
	int i, j;

	for(i=0; i<10; i++) {
		for(j=0; j<10; j++) {
			B[i][j] = 0.0;
		}
		B[i][i] = a[i];
	}

	return 0;
}

int mat4_4_diag(double a[4], double B[4][4]) {
	int i, j;

	for(i=0; i<4; i++) {
		for(j=0; j<4; j++) {
			B[i][j] = 0.0;
		}
		B[i][i] = a[i];
	}

	return 0;
}

int mat4_1_scale(double a[4], double b, double c[4]) {
	int i;

	for(i=0; i<4; i++) {
		c[i] = a[i] * b;
	}

	return 0;
}

int mat3_1_copy(double a[3], double b[3]) {
	b[0] = a[0];
	b[1] = a[1];
	b[2] = a[2];

	return 0;
}

int mat3_1_scale(double a[3], double b, double c[3]) {
	c[0] = a[0]*b;
	c[1] = a[1]*b;
	c[2] = a[2]*b;

	return 0;
}

int transformation_inv(double A[4][4], double B[4][4]) {
	double p_new[3];	// new position vector
	int i, j;

	// R_new = R^T
	// p_new = -R^T * p;
	for(i=0; i<3; i++) {
		p_new[i] = 0.0;
		for(j=0; j<3; j++) {
			B[i][j] = A[j][i];	// transpose
			p_new[i] -= A[j][i]*A[j][3];	// new position vector
		}
		B[i][3] = p_new[i];
	}
	B[3][0] = 0.0; B[3][1] = 0.0; B[3][2] = 0.0; B[3][3] = 1.0;

	return 0;
}

int cross_product(double a[3], double b[3], double c[3]) {
	c[0] = a[1]*b[2] - a[2]*b[1];
	c[1] = a[2]*b[0] - a[0]*b[2];
	c[2] = a[0]*b[1] - a[1]*b[0];

	return 0;
}		

int init_WP(WP *wp, int sup_leg) {

	wp->sup_leg = sup_leg;
	if(sup_leg == mwpg.RIGHT) {
		wp->WS[0] = 0.0;
		wp->WS[1] = 0.0;
		wp->WS[2] = mwpg.PELVIS_LENGTH*0.5;
		wp->WS[3] = 0.0;

		wp->FS[0] = 0.0;
		wp->FS[1] = 0.0;
		wp->FS[2] = mwpg.PELVIS_LENGTH;
		wp->FS[3] = 0.0;
		wp->FS[4] = 0.0;
		wp->FS[5] = 0.0;
	}
	else {
		wp->WS[0] = 0.0;
		wp->WS[1] = 0.0;
		wp->WS[2] = -mwpg.PELVIS_LENGTH*0.5;
		wp->WS[3] = 0.0;

		wp->FS[0] = 0.0;
		wp->FS[1] = 0.0;
		wp->FS[2] = -mwpg.PELVIS_LENGTH;
		wp->FS[3] = 0.0;
		wp->FS[4] = 0.0;
		wp->FS[5] = 0.0;
	}
	wp->CM_R[0] = 0.0;
	wp->CM_R[1] = 0.0;
	wp->CC[0] = 0.0;
	wp->CC[1] = 0.0;
	wp->F_R[0] = 0.0;
	wp->F_R[1] = 0.0;
	wp->state = mwpg.DS_START;
	wp->T_ds_remain = mwpg.T_ds_start;

	return 0;
}

int ready_position(double q_init[12], REF *ref) {
	WP wp;
	int i;
	int index;

	init_mwpg();
	init_WP(&wp, mwpg.RIGHT);
	*ref = forward_kinematics(&wp);
	index = 0;
	for(i=0; i<2; i++) {
		q_init[index++] = ref->hipy_q[i];
		q_init[index++] = ref->hipx_q[i];
		q_init[index++] = ref->hipz_q[i];
		q_init[index++] = ref->kneey_q[i];
		q_init[index++] = ref->ankley_q[i];
		q_init[index++] = ref->anklex_q[i];
	}

	//for(i=0; i<12; i++) printf("q_init[%d]: %f \t", i, q_init[i]);
	//printf("\n");

	return 0;
}






////////////////////////////////////////////////////////////////////////

REF forward_kinematics(WP *wp) {

	REF ref;
	int i, j;

	// transformation matrix
	double T_CM[4][4];	// CM coord. frame w.r.t. supporting sole
	double T_SW[4][4];	// swing sole coord. frame w.r.t supporting sole
	double T_SOLE[2][4][4];	// left/right swing sole coord. frame 
							// w.r.t left/right hip

	double T_CM2LHIP[4][4];	// left hip coord. frame w.r.t. CM coord. frame
	double T_CM2RHIP[4][4];	// right hip coord. frame w.r.t. CM coord. frame

	double T1[4][4], T2[4][4];
	double p[3];
	double c_q, s_q;
	double q;


      
	int index;

	// geometrical inverse kinematics
	double d[3], v[3], w[3], n[3], r[3], u[3], l[3], x02[3], v_cross_w[3];
  double cos_alpha, sin_alpha, cos_beta;
  double u_mag, l_mag, d_mag, v_mag, n_mag, r_mag; 
  double tmp;

  u_mag = mwpg.THIGH_LENGTH;
  l_mag = mwpg.SHIN_LENGTH;
  d_mag = mwpg.SOLE2ANKLE;
  
	
	// CM transformation matrix w.r.t. supporting sole
	q = wp->CM_R[0];
	c_q = cos(q);
	s_q = sin(q);
	T_CM[0][0] = c_q; T_CM[0][1] = -s_q; T_CM[0][2] = 0.0; T_CM[0][3] = wp->WS[0];
	T_CM[1][0] = s_q; T_CM[1][1] =  c_q; T_CM[1][2] = 0.0; T_CM[1][3] = wp->WS[2];
	T_CM[2][0] = 0.0; T_CM[2][1] =  0.0; T_CM[2][2] = 1.0; T_CM[2][3] = mwpg.Z_c;
	T_CM[3][0] = 0.0; T_CM[3][1] =  0.0; T_CM[3][2] = 0.0; T_CM[3][3] = 1.0;

	// swing sole transformation matrix w.r.t supporting sole
	q = wp->F_R[0];
	c_q = cos(q);
	s_q = sin(q);
	T_SW[0][0] = c_q; T_SW[0][1] = -s_q; T_SW[0][2] = 0.0; T_SW[0][3] = wp->FS[0];
	T_SW[1][0] = s_q; T_SW[1][1] =  c_q; T_SW[1][2] = 0.0; T_SW[1][3] = wp->FS[2];
	T_SW[2][0] = 0.0; T_SW[2][1] =  0.0; T_SW[2][2] = 1.0; T_SW[2][3] = wp->FS[4];
	T_SW[3][0] = 0.0; T_SW[3][1] =  0.0; T_SW[3][2] = 0.0; T_SW[3][3] = 1.0;
  

	// left hip coord. frame w.r.t CM coord. frame
	T_CM2LHIP[0][0] = 1.0; T_CM2LHIP[0][1] = 0.0; T_CM2LHIP[0][2] = 0.0; T_CM2LHIP[0][3] = -mwpg.HIP_CENTER2CM[0];
	T_CM2LHIP[1][0] = 0.0; T_CM2LHIP[1][1] = 1.0; T_CM2LHIP[1][2] = 0.0; T_CM2LHIP[1][3] = mwpg.PELVIS_LENGTH*0.5;
	T_CM2LHIP[2][0] = 0.0; T_CM2LHIP[2][1] = 0.0; T_CM2LHIP[2][2] = 1.0; T_CM2LHIP[2][3] = -mwpg.HIP_CENTER2CM[2];
	T_CM2LHIP[3][0] = 0.0; T_CM2LHIP[3][1] = 0.0; T_CM2LHIP[3][2] = 0.0; T_CM2LHIP[3][3] = 1.0;

	// right hip coord. frame w.r.t CM coord. frame
	T_CM2RHIP[0][0] = 1.0; T_CM2RHIP[0][1] = 0.0; T_CM2RHIP[0][2] = 0.0; T_CM2RHIP[0][3] = -mwpg.HIP_CENTER2CM[0];
	T_CM2RHIP[1][0] = 0.0; T_CM2RHIP[1][1] = 1.0; T_CM2RHIP[1][2] = 0.0; T_CM2RHIP[1][3] = -mwpg.PELVIS_LENGTH*0.5;
	T_CM2RHIP[2][0] = 0.0; T_CM2RHIP[2][1] = 0.0; T_CM2RHIP[2][2] = 1.0; T_CM2RHIP[2][3] = -mwpg.HIP_CENTER2CM[2];
	T_CM2RHIP[3][0] = 0.0; T_CM2RHIP[3][1] = 0.0; T_CM2RHIP[3][2] = 0.0; T_CM2RHIP[3][3] = 1.0;

	// compute transformation matrix from each hip coord. frame 
	// to the corresponding sole coord. frame
	if(wp->sup_leg == mwpg.LEFT) {
		// for left supporting leg
		// left first!
		mat4_4_4_mult(T_CM, T_CM2LHIP, T1);
		transformation_inv(T1, T_SOLE[mwpg.LEFT]);
		// then right
		mat4_4_4_mult(T_CM, T_CM2RHIP, T1);
		transformation_inv(T1, T2);
		mat4_4_4_mult(T2, T_SW, T_SOLE[mwpg.RIGHT]);
    
    Le_x = 0.0;
    Re_x = wp->FS[0];
    Le_y = 0.0;
    Re_y = -mwpg.PELVIS_LENGTH;
    
    F_X = wp->FS[0]*100.;
    F_Y = wp->FS[2]*100.;
    F_Z = wp->FS[4]*100.;
    F_R = wp->F_R[0];
    
    S_leg = mwpg.LEFT;
    

	}
	else {
		// for right supporting leg
		// left first!
		mat4_4_4_mult(T_CM, T_CM2LHIP, T1);
		transformation_inv(T1, T2);
		mat4_4_4_mult(T2, T_SW, T_SOLE[mwpg.LEFT]);
		// then right
		mat4_4_4_mult(T_CM, T_CM2RHIP, T1);
		transformation_inv(T1, T_SOLE[mwpg.RIGHT]);
    
    Le_x = wp->FS[0];  
    Re_x = 0.0;  
    Le_y = mwpg.PELVIS_LENGTH;
    Re_y = 0.0;

    F_X = wp->FS[0]*100.;
    F_Y = wp->FS[2]*100.;
    F_Z = wp->FS[4]*100.;
    F_R = wp->F_R[0];
    
    S_leg = mwpg.RIGHT;

	}
	//printf("T_SOLE[0][0][3]=%f \t T_SOLE[0][1][3]=%f \t T_SOLE[0][2][3]=%f \t", 
	//	T_SOLE[0][0][3], T_SOLE[0][1][3], T_SOLE[0][2][3]);
	//printf("\nT_SOLE[1][0][3]=%f \t T_SOLE[1][1][3]=%f \t T_SOLE[1][2][3]=%f \t", 
	//	T_SOLE[1][0][3], T_SOLE[1][1][3], T_SOLE[1][2][3]);
  
  //cout << Le_x << "\t" << Re_x << endl;
  
  
  
	for(i=0; i<2; i++) {
		if(i==0) index = mwpg.LEFT;
		else index = mwpg.RIGHT;

		// compute v vector
		for(j=0; j<3; j++) {			
			w[j] = T_SOLE[index][j][0];
			d[j] = d_mag*T_SOLE[index][j][2];
			p[j] = T_SOLE[index][j][3];
			v[j] = p[j] + mwpg.SOLE2ANKLE*d[j];
		}
    //cout << p[0] << "\t" << p[1] << "\t" << p[2] <<  endl;
		
    v_mag = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
    
    
		// compute n vector
		cross_product(v, w, n);
    n_mag = sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);
    //cout << n[0] << "\t" << n[1] << "\t" << n[2] <<  endl;
	
    
    // compute r vector
		cross_product(n, v, r);
    r_mag = sqrt(r[0]*r[0] + r[1]*r[1] + r[2]*r[2]);
    //cout << r[0] << "\t" << r[1] << "\t" << r[2] <<  endl;
    
   
    // compute cos_alpha 
    cos_alpha = ( v_mag*v_mag + u_mag*u_mag - l_mag*l_mag) / (2.0*v_mag*u_mag);
    sin_alpha = sqrt(1.0 - cos_alpha*cos_alpha);
   
    // compute u vector
    u[0] = u_mag * ( v[0]*cos_alpha/v_mag + r[0]*sin_alpha/r_mag );
    u[1] = u_mag * ( v[1]*cos_alpha/v_mag + r[1]*sin_alpha/r_mag );
    u[2] = u_mag * ( v[2]*cos_alpha/v_mag + r[2]*sin_alpha/r_mag );
    //cout << u[0] << "\t" << u[1] << "\t" << u[2] <<  endl;

    // compute l vector
    l[0] = v[0] - u[0];
    l[1] = v[1] - u[1];
    l[2] = v[2] - u[2];
    //cout << l[0] << "\t" << l[1] << "\t" << l[2] <<  endl;


    // compute cos_beta 
    cos_beta = (l[0]*w[0] + l[1]*w[1] + l[2]*w[2]) / l_mag ;
    

    // compute x02 vector
    x02[0] = -u[2] / sqrt(u[0]*u[0] + u[2]*u[2]);
    x02[1] = 0.0;
    x02[2] = u[0] / sqrt(u[0]*u[0] + u[2]*u[2]);
    
		// angle
		ref.hipy_q[index] = atan2(-u[0], -u[2]);
		ref.hipx_q[index] = asin(u[1]/u_mag);
		ref.hipz_q[index] = PI/2.0 - acos( (x02[0]*n[0] + x02[1]*n[1] + x02[2]*n[2])/n_mag );
		ref.kneey_q[index] = acos( (u[0]*l[0] + u[1]*l[1] + u[2]*l[2]) / (u_mag*l_mag) );
		ref.ankley_q[index] = PI/2.0 - acos(cos_beta);
		ref.anklex_q[index] = PI/2.0 - acos( (n[0]*d[0] + n[1]*d[1] + n[2]*d[2])/(n_mag*d_mag) );


    
    //cout << ref.hipy_q[index] *180.0/PI << "\t" << ref.hipx_q[index] *180.0/PI << "\t" << ref.hipz_q[index] *180.0/PI << "\t" << ref.kneey_q[index] *180.0/PI<< "\t" << ref.ankley_q[index] *180.0/PI<< "\t" << ref.anklex_q[index] *180.0/PI<< endl;
    //cout << acos( (n[0]*d[0] + n[1]*d[1] + n[2]*d[2])/(n_mag*d_mag) )*180./PI << endl;
    //cout << ref.anklex_q[index] *180.0/PI<< endl;
    
    //cout << d[0] << "\t" << d[1] << "\t" << d[2] <<  endl;
		//cout << n[0] << "\t" << n[1] << "\t" << n[2] <<  endl;
    //out << v[0] << "\t" << v[1] << "\t" << v[2] <<  endl;
    //cout << p[0] << "\t" << p[1] << "\t" << p[2] <<  endl;
    
    /*
		ref.hipz_q[index] = 0.0;
		ref.hipx_q[index] = -10.0/180.0*PI;
		ref.hipy_q[index] = -30.0/180.0*PI;
		ref.kneey_q[index] = 60.0/180.0*PI;
		ref.ankley_q[index] = -30.0/180.0*PI;
		ref.anklex_q[index] = 10.0/180.0*PI;
    */

		
		// angular velocity
		ref.hipz_qd[index] = 0.0;
		ref.hipx_qd[index] = 0.0;
		ref.hipy_qd[index] = 0.0;
		ref.kneey_qd[index] = 0.0;
		ref.ankley_qd[index] = 0.0;
		ref.anklex_qd[index] = 0.0;
		
}

	return ref;
}




int wp_copy(WP *w1, WP *w2) {

	mat4_1_copy(w1->WS, w2->WS);
	mat2_1_copy(w1->CM_R, w2->CM_R);
	mat6_1_copy(w1->FS, w2->FS);
	//mat2_1_copy(w1->FS_R, w2->FS_R);
	mat2_1_copy(w1->F_R, w2->F_R);
	mat2_1_copy(w1->CC, w2->CC);
	w2->sup_leg = w1->sup_leg;
	w2->state = w1->state;
	w2->T_ds_remain = w1->T_ds_remain;

	return 0;
}

int init_mwpg() {

//	double a[10], b[4];

	mwpg.PELVIS_LENGTH = 0.2;//0.2;
	mwpg.THIGH_LENGTH = 0.25;//0.25;
	mwpg.SHIN_LENGTH = 0.25;//0.25;

	mwpg.Z_c = 0.8;//0.70;
	mwpg.HIP_CENTER2CM[0] = 0.0;
	mwpg.HIP_CENTER2CM[1] = 0.0;	
	mwpg.HIP_CENTER2CM[2] = 0.30;//0.15;
	mwpg.SOLE2ANKLE = 0.1;//LFOOT_CM2LANKLE[2] + (-LFOOT_CM2FL[2]);
	mwpg.gravity = grav;
	mwpg.T_c = sqrt(mwpg.Z_c / mwpg.gravity);

	mwpg.T_samples = T_ctrl;


	mwpg.P_MAX = 0.1;
	mwpg.P_MIN = -0.1;
	mwpg.Q_MAX = 0.1;
	mwpg.Q_MIN = -mwpg.Q_MAX;


/*
	mwpg.P_MAX = 0.005;
	mwpg.P_MIN = -0.005;
	mwpg.Q_MAX = 0.005;
	mwpg.Q_MIN = -mwpg.Q_MAX;
*/  

	mwpg.CS_STOP[0] = 0.8;//0.4;
	mwpg.CS_STOP[1] = 0.8;//0.4; 
	mwpg.CS_STOP[2] = 0.4;//0.1;
	mwpg.CS_STOP[3] = 0.4;//0.1;
	mwpg.CS_STOP[4] = 0.0;
	mwpg.CS_STOP[5] = 0.0;
	mwpg.CS_STOP[6] = 0.2;//0.2;
	mwpg.CS_STOP[7] = -0.2;//-0.2;
	mwpg.CS_STOP[8] = 0.0;
	mwpg.CS_STOP[9] = 0.0;

	mwpg.Rc = 0.02;

	mwpg.FIW = 0.1;
	mwpg.FOW = 0.1;
	mwpg.FFL = 0.05;
	mwpg.FBL = 0.05;

	mwpg.T_ds_start = 0.6;
	mwpg.T_ds_stop = 0.3;

	mwpg.T_SS_MAX = 1.0;
	
	mwpg.DS_START = 0;
	mwpg.SS = 1;
	mwpg.DS = 2;
	mwpg.DS_STOP = 3;
	mwpg.STOP = 4;

	mwpg.LEFT = 0;
	mwpg.RIGHT = 1;
	mwpg.TRUE = 1;
	mwpg.FALSE = 0;

	mwpg.NEAR_ZERO = 1e-10;
	mwpg.PLUS_TOLERANCE = 1;
	mwpg.MINUS_TOLERANCE = -1;

	mwpg.F_XY_ACC_MAX = mwpg.gravity * 10.0;
	mwpg.F_Z_ACC_MAX = mwpg.gravity * 10.0;

	return 0;
}





int walking_pattern(REF *ref) {
	//double CS_raw[10], double CS_mod[10], double CS_old[10],
	//WP wp_k, WP wp_kp1) {


	static int num_of_steps = 0;
	static int is_first = 1;
	static double CS[10], CS_mod[10], CS_old[10];
	static int stop_flag;
	static WP wp_k, wp_kp1;
	static int index = 0;

	double CS_dbg[10];

	if(is_first) {
		init_mwpg();
		init_WP(&wp_k, mwpg.RIGHT);
		mat10_1_copy(CS_list[0], CS);
		mat10_1_copy(CS,CS_old);
		stop_flag = mwpg.FALSE;
		is_first = 0;
		dbg(123);
	}
	//dbg(321);
	// state 가 STOP 이면 walking pattern 을 계산하지 않는다.
	//dbg(wp_k.state);
	mat10_1_copy(CS, CS_dbg);
  
  

   
   
	if(wp_k.state != mwpg.STOP) {
		
		compute_reference_CM_foot_motion(CS, CS_old, &wp_k, stop_flag, &wp_kp1, CS_mod);
		//printf("WS[0]=%f\t WS[1]=%f\t WS[2]=%f\n", wp_kp1.WS[0], wp_kp1.WS[1], wp_kp1.WS[2]);
		//printf("FS[0]=%f\t FS[2]=%f\t FS[4]=%f\n", wp_kp1.FS[0], wp_kp1.FS[2], wp_kp1.FS[4]);

   
		if(wp_kp1.state != mwpg.DS_STOP) {
			if( (wp_kp1.state != wp_k.state) && (wp_kp1.state == mwpg.DS) ) {
				num_of_steps++;
        
				if (num_of_steps > TOTAL_NUM_OF_STEPS) {
					mat10_1_copy(mwpg.CS_STOP, CS);
					stop_flag = mwpg.TRUE;
				}
				else {
					mat10_1_copy(CS_list[num_of_steps-1], CS);
				}
			}
			else if(wp_kp1.state == mwpg.SS) {
				mat10_1_copy(CS_mod, CS_old);
				mat10_1_copy(CS_mod, CS);
			}
		}

	}
  else
    exit(0);
            
	if(CS[0] < mwpg.NEAR_ZERO){
		dbg(124);
	}

	// forward kinematics 계산
	*ref = forward_kinematics(&wp_kp1);
	
	// update WP
	wp_copy(&wp_kp1, &wp_k);

	if(index++%10==0)
	//printf("%d\t walking state: %d\n", index, wp_kp1.state);
	

	/*
	printf("%d:\t walking state: %d\t", index++, wp_kp1.state);
	printf("WP: WS[0] = %2.10f \t WS[1] = %2.10f \t WS[2] = %2.10f \t WS[3] = %2.10f \t CC[0] = %2.10f \t CC[1] = %2.10f \n",
		wp_k.WS[0], wp_k.WS[1], wp_k.WS[2], wp_k.WS[3], wp_k.CC[0], wp_k.CC[1]);
	*/
           
  //cout << wp_k.state << endl;  
  
  

  
	return wp_k.state;	// state를 리턴
}

int change_supporting_leg(WP *w_old, WP *w_new) {
	//double WS_old[4], double CM_R_old[2], double FS_old[4], double F_R_old[2], 
	//int sup_leg_old,
	//double WS_new[4], double CM_R_new[2], double FS_new[4], double F_R_new[2], 
	//int *sup_leg_new) {
	// double WS_new[4], double CM_R_new[2], double FS_new[4], double F_R_new[2]) {
	//double WS[4], double CM_R[2], double FS[4], double F_R[2], 
	//double WS_new[4], double CM_R_new[2], double FS_new[4], double F_R_new[2]) {

	double F_q;
	double R[2][2];
	double p[2], p_new[2];
	double R_new[2][2];
	double CM_pos[2], CM_vel[2], F_vel[2];
	double Tmp2_2[2][2], Tmp2_1[2];
	double CM_pos_new[2], CM_vel_new[2];
	double F_pos_new[2], F_vel_new[2];
	double CM_q, CM_dq;
	double F_dq;
	double CM_q_new, CM_dq_new;
	double F_q_new, F_dq_new;

	F_q = w_old->F_R[0];
	R[0][0] = cos(F_q);	R[0][1] = -sin(F_q);
	R[1][0] = sin(F_q);	R[1][1] = cos(F_q);
	
	p[0] = w_old->FS[0];
	p[1] = w_old->FS[2];
  

    
	mat2_2_transpose(R, R_new);
	
	mat2_2_scale(R_new, -1.0, Tmp2_2);
	mat2_2_1_mult(Tmp2_2, p, p_new);

	CM_pos[0] = w_old->WS[0]; CM_pos[1] = w_old->WS[2];
	CM_vel[0] = w_old->WS[1]; CM_vel[1] = w_old->WS[3];

	F_vel[0] = w_old->FS[1]; F_vel[1] = w_old->FS[3];

	mat2_2_1_mult(R_new, CM_pos, Tmp2_1);
	mat2_1_add(Tmp2_1, p_new, CM_pos_new);

	mat2_2_1_mult(R_new, CM_vel, CM_vel_new);

	mat2_1_copy(p_new, F_pos_new);
	mat2_2_1_mult(R_new ,F_vel, F_vel_new);

	CM_q = w_old->CM_R[0];
	CM_dq = w_old->CM_R[1];

	F_dq = w_old->F_R[1];

	CM_q_new = CM_q - F_q;
	CM_dq_new = CM_dq - F_dq;

	F_q_new = -F_q;
	F_dq_new = -F_dq;

	// 지금은 평지 걸음만 고려하며, supporting leg가 바귀는 순간은 
	// 언제나 double support 라고 가정한다.
	// 따라서 발의 높이와 속도, CC 값을 모두 초기화(=0) 시킨다.
	// 추후, 경사면을 고려하게 되면, 다시 계산해야 한다.
	w_new->WS[0] = CM_pos_new[0]; 
	w_new->WS[1] = CM_vel_new[0];
	w_new->WS[2] = CM_pos_new[1];
	w_new->WS[3] = CM_vel_new[1];
	w_new->CM_R[0] = CM_q_new;
	w_new->CM_R[1] = CM_dq_new;
	w_new->FS[0] = F_pos_new[0];
	w_new->FS[1] = F_vel_new[0];
	w_new->FS[2] = F_pos_new[1];
	w_new->FS[3] = F_vel_new[1];
	w_new->FS[4] = 0.0;
	w_new->FS[5] = 0.0;
	w_new->F_R[0] = F_q_new;
	w_new->F_R[1] = F_dq_new;
	w_new->CC[0] = 0.0;
	w_new->CC[1] = 0.0;

	if(w_old->sup_leg == mwpg.RIGHT) w_new->sup_leg = mwpg.LEFT;
	else w_new->sup_leg = mwpg.RIGHT;

	if(w_new->WS[1] > 100) {
		printf("BBB\n");
	}

	return 0;
}

int compute_P_function( 
	double x_k, double v_k, double x_d, double v_d, 
	double *T_ss, double *P) {

	double T_c;
//	double T_ss_tmp;
	double det;

	T_c = mwpg.T_c;

	//T_ss_tmp = ((v_d + v_k) + (x_d - x_k)) / ((v_d + v_k) - (x_d - x_k));
	det = (x_k - x_d)*(x_k - x_d) + (v_k - v_d)*(v_k - v_d);
	if (det < mwpg.NEAR_ZERO) {
		*T_ss = 0.0;
		*P = 0.0;
	}
	else {
		*T_ss = T_c*log( ((v_d + v_k) + (x_d - x_k)) / ((v_d + v_k) - (x_d - x_k)) );
		*P = ( (x_d*x_d - x_k*x_k) - (v_d*v_d - v_k*v_k) )/(2.0*(x_d - x_k));
	}

	return 0;
}


int compute_Q_function( 
	double y_k, double w_k, double y_d, double w_d, 
	double T, double *T_sw, double *Q) {

	double NEAR_ZERO;
	double T_c;
	double C, S;
	double y_h, w_h;
	double y_p, w_p;
	double alpha, beta, gamma, delta;
	double h;
	double det;

	NEAR_ZERO = mwpg.NEAR_ZERO;
	T_c = mwpg.T_c;

	C = cosh(T/T_c);
	S = sinh(T/T_c);

	y_h = C*y_k + S*w_k;
	w_h = S*y_k + C*w_k;

	y_p = y_d - y_h;
	w_p = w_d - w_h;

	alpha = y_p - w_p;
	beta = y_p + w_p;
	gamma = y_p*S - w_p*(1+C);
	delta = y_p*S + w_p*(1-C);

	if (fabs(delta) < NEAR_ZERO) {
		h = 1.0;
	}
	else if(delta < 0.0) {
		if (fabs(alpha) < NEAR_ZERO) {
			h = -beta/gamma;
		}
		else {
			h = (gamma - sqrt(gamma*gamma + 4.0*alpha*beta))/(2.0*alpha);
		}
	}
	else {
		if (fabs(alpha) < NEAR_ZERO) {
			h = -beta/gamma;
		}
		else {
			h = (gamma + sqrt(gamma*gamma + 4.0*alpha*beta))/(2.0*alpha);
		}
	}

	*T_sw = T - T_c*log(h);
	det = h + 1.0/h - 1.0 - C;
	if (fabs(det) > NEAR_ZERO) {
		*Q = y_p/det;
	}
	else {
		*Q = 0.0;
	}

	return 0;
}


int check_sagittal_feasibility(
	double T_ss, double P, double T_SS_MAX, 
	double T_SS_MIN, int tolerance_sign) {  

	int TRUE, FALSE;
	double NEAR_ZERO;
	double P_MAX, P_MIN;
	int isFeasible;
	double tolerance;

	TRUE = mwpg.TRUE;
	FALSE = mwpg.FALSE;
	NEAR_ZERO = mwpg.NEAR_ZERO;
	P_MAX = mwpg.P_MAX;
	P_MIN = mwpg.P_MIN;

	if (tolerance_sign == mwpg.PLUS_TOLERANCE) {
		tolerance = mwpg.NEAR_ZERO;
	}
	else {
		tolerance = -mwpg.NEAR_ZERO;
	}
	
	if (isnan(T_ss)) {
		isFeasible = FALSE;
	}
	else if ( (T_ss < (T_SS_MIN - tolerance)) || (T_ss > (T_SS_MAX + tolerance))
		|| (P > (P_MAX + tolerance)) || (P < (P_MIN - tolerance) )) {
		isFeasible = FALSE;
	}
	else {
		isFeasible = TRUE;
	}

	return isFeasible;
}


int check_lateral_feasibility(
	double T_ss, double T_sw, double Q,
	int tolerance_sign) {

	int TRUE, FALSE;
	int isFeasible;
	double Q_MAX, Q_MIN;
	double NEAR_ZERO;
	double tolerance;

	TRUE = mwpg.TRUE;
	FALSE = mwpg.FALSE;
	Q_MAX = mwpg.Q_MAX;
	Q_MIN = mwpg.Q_MIN;
	NEAR_ZERO = mwpg.NEAR_ZERO;

	if(tolerance_sign == mwpg.PLUS_TOLERANCE) {
		tolerance = mwpg.NEAR_ZERO;
	}
	else {
		tolerance = -mwpg.NEAR_ZERO;
	}

	if (isnan(T_sw)) {
		isFeasible = FALSE;
	}
	else if ( (T_sw < 0.0) || (T_sw > T_ss) || 
		(Q > (Q_MAX + tolerance)) || (Q < (Q_MIN - tolerance) ) ) {
		isFeasible = FALSE;
	}
	else {
		isFeasible = TRUE;
	}

	return isFeasible;
}


int check_CM_feasibility(
	double WS_k[4], double WS_d[4], 
	double T_SS_MAX, double T_SS_MIN, int tolerance_sign,
	int *isSagittalFeasible, int *isLateralFeasible, 
	double *T_ss, double *P, double *T_sw, double *Q) {

	double x_k, v_k, y_k, w_k;
	double x_d, v_d, y_d, w_d;


	x_k = WS_k[0];
	v_k = WS_k[1];
	y_k = WS_k[2];
	w_k = WS_k[3];

	x_d = WS_d[0];
	v_d = WS_d[1];
	y_d = WS_d[2];
	w_d = WS_d[3];

	compute_P_function(x_k, v_k, x_d, v_d, T_ss, P);
	compute_Q_function(y_k, w_k, y_d, w_d, *T_ss, T_sw, Q);

	*isSagittalFeasible = check_sagittal_feasibility(*T_ss, *P, T_SS_MAX, 
		T_SS_MIN, tolerance_sign);
	*isLateralFeasible = check_lateral_feasibility(*T_ss, *T_sw, *Q,
		tolerance_sign);
	if( *isSagittalFeasible == 0 ) {
			//printf("sagittal error\n");
	}
	if( *isLateralFeasible == 0 ) {
			//printf("latteral error\n");
	}
	return 0;
}


int compute_CM_ds_motion(double WS_k[4], double t, double WS_kp1[4]) {

	double T_c;
	double x_k, v_k, y_k, w_k;
	double x_kp1, v_kp1, y_kp1, w_kp1;

	T_c = mwpg.T_c;
	
	x_k = WS_k[0];
	v_k = WS_k[1];
	y_k = WS_k[2];
	w_k = WS_k[3];

	x_kp1 = x_k + v_k/T_c*t;
	v_kp1 = v_k;
	y_kp1 = y_k + w_k/T_c*t;
	w_kp1 = w_k;

	WS_kp1[0] = x_kp1;
	WS_kp1[1] = v_kp1;
	WS_kp1[2] = y_kp1;
	WS_kp1[3] = w_kp1;

	return 0;
}


int compute_CM_motion_with_P_function(
	double P, double pos_k, double vel_k, double time, 
	double *pos_kp1, double *vel_kp1) {

	double T_c;
	double C_t, S_t;
	double pos_h, vel_h;
	double pos_p, vel_p;

	T_c = mwpg.T_c;
	C_t = cosh(time/T_c);
	S_t = sinh(time/T_c);

	pos_h = C_t*pos_k + S_t*vel_k;
	vel_h = S_t*pos_k + C_t*vel_k;

	pos_p = P*(1.0 - C_t);
	vel_p = -P*S_t;

	*pos_kp1 = pos_h + pos_p;
	*vel_kp1 = vel_h + vel_p;

	return 0;
}


int compute_CM_motion_with_Q_function(
	double T_sw, double Q, double pos_k, double vel_k, double time,
	double *pos_kp1, double *vel_kp1) {
		
	double T_c;
	double C_t, S_t;
	double pos_h, vel_h;
	double pos_p, vel_p;
	
	T_c = mwpg.T_c;

	C_t = cosh(time/T_c);
	S_t = sinh(time/T_c);

	pos_h = C_t*pos_k + S_t*vel_k;
	vel_h = S_t*pos_k + C_t*vel_k;

	if (T_sw < time) {
		pos_p = Q*(2.0*cosh((time - T_sw)/T_c) - (1.0 + C_t));
		vel_p = Q*(2.0*sinh((time - T_sw)/T_c) - S_t);
	}
	else {
		pos_p = Q*(1.0 - C_t);
		vel_p = -Q*S_t;
	}

	*pos_kp1 = pos_h + pos_p;
	*vel_kp1 = vel_h + vel_p;

	return 0;
}


int compute_CM_ss_motion(
	double T_ss, double P, double T_sw, double Q, double WS_k[4], double time, 
	double WS_kp1[4]) {

	double x_k, v_k, y_k, w_k;
	double x_kp1, v_kp1, y_kp1, w_kp1;

	x_k = WS_k[0];
	v_k = WS_k[1];
	y_k = WS_k[2];
	w_k = WS_k[3];

	compute_CM_motion_with_P_function(P, x_k, v_k, time, &x_kp1, &v_kp1);
	compute_CM_motion_with_Q_function(T_sw, Q, y_k, w_k, time, &y_kp1, &w_kp1);

	WS_kp1[0] = x_kp1; WS_kp1[1] = v_kp1; WS_kp1[2] = y_kp1; WS_kp1[3] = w_kp1;

	return 0;
}


int compute_desired_WS(
	double CS[10], int supporting_leg, double WS_d[4]) {

	double E44[4][4];
	double T_sl, T_sr, T_dl, T_dr, F_l, F_r, S_l, S_r, theta_l, theta_r;
	double T_c;
	double A_T_sl[2][2], A_T_sr[2][2], R_theta_l[2][2], R_theta_r[2][2], U_T_dl[2][2], U_T_dr[2][2], D_l[2][2], D_r[2][2];
	double cosh_T_sl, sinh_T_sl;
	double cosh_T_sr, sinh_T_sr;
	double cos_theta_l, sin_theta_l;
	double cos_theta_r, sin_theta_r;
	double Astar_r[4][4], Astar_l[4][4];
	double Astar0_r[4][4], Astar0_l[4][4];
	double Tmp_a_2_2[2][2], Tmp_b_2_2[2][2];
	double bstar_r_tmp0[2][2], bstar_r_tmp1[2][2];
	double bstar_l_tmp0[2][2], bstar_l_tmp1[2][2];
	double bstar_r[4], bstar_l[4];
	double det_l, det_r;
	double Tmp_a_4_4[4][4];
	double Tmp_a_4_1[4], Tmp_b_4_1[4];
	double NEAR_ZERO;
	int LEFT, RIGHT;
	
	E44[0][0] = 1.0; E44[0][1] = 0.0; E44[0][2] = 0.0; E44[0][3] = 0.0;
	E44[1][0] = 0.0; E44[1][1] = 1.0; E44[1][2] = 0.0; E44[1][3] = 0.0;
	E44[2][0] = 0.0; E44[2][1] = 0.0; E44[2][2] = 1.0; E44[2][3] = 0.0;
	E44[3][0] = 0.0; E44[3][1] = 0.0; E44[3][2] = 0.0; E44[3][3] = 1.0;

	T_sl = CS[0];
	T_sr = CS[1];
	T_dl = CS[2];
	T_dr = CS[3];
	F_l = CS[4];
	F_r = CS[5];
	S_l = CS[6];
	S_r = CS[7];
	theta_l = CS[8];
	theta_r = CS[9];

	T_c = mwpg.T_c;

	cosh_T_sl = cosh(T_sl/T_c);
	sinh_T_sl = sinh(T_sl/T_c);
	A_T_sl[0][0] = cosh_T_sl; A_T_sl[0][1] = sinh_T_sl;
	A_T_sl[1][0] = sinh_T_sl; A_T_sl[1][1] = cosh_T_sl;

	cosh_T_sr = cosh(T_sr/T_c);
	sinh_T_sr = sinh(T_sr/T_c);
	A_T_sr[0][0] = cosh_T_sr; A_T_sr[0][1] = sinh_T_sr;
	A_T_sr[1][0] = sinh_T_sr; A_T_sr[1][1] = cosh_T_sr;

	cos_theta_l = cos(theta_l);
	sin_theta_l = sin(theta_l);
	R_theta_l[0][0] = cos_theta_l; R_theta_l[0][1] = -sin_theta_l;
	R_theta_l[1][0] = sin_theta_l; R_theta_l[1][1] = cos_theta_l;

	cos_theta_r = cos(theta_r);
	sin_theta_r = sin(theta_r);
	R_theta_r[0][0] = cos_theta_r; R_theta_r[0][1] = -sin_theta_r;
	R_theta_r[1][0] = sin_theta_r; R_theta_r[1][1] = cos_theta_r;

	U_T_dl[0][0] = 1.0; U_T_dl[0][1] = T_dl/T_c;
	U_T_dl[1][0] = 0.0; U_T_dl[1][1] = 1.0;

	U_T_dr[0][0] = 1.0; U_T_dr[0][1] = T_dr/T_c;
	U_T_dr[1][0] = 0.0; U_T_dr[1][1] = 1.0;

	D_l[0][0] = F_l; D_l[0][1] = S_l;
	D_l[1][0] = 0.0; D_l[1][1] = 0.0;

	D_r[0][0] = F_r; D_r[0][1] = S_r;
	D_r[1][0] = 0.0; D_r[1][1] = 0.0;

	mat2_2_transpose(R_theta_l, Tmp_a_2_2);
	mat2_2_2_mult(A_T_sl, U_T_dr, Tmp_b_2_2);
	mat2_2_to_kronecker_product(Tmp_a_2_2, Tmp_b_2_2, Astar_r);
	mat2_2_transpose(R_theta_r, Tmp_a_2_2);
	mat2_2_2_mult(A_T_sr, U_T_dl, Tmp_b_2_2);
	mat2_2_to_kronecker_product(Tmp_a_2_2, Tmp_b_2_2, Astar_l);

	mat2_2_2_mult(A_T_sl, D_l, bstar_r_tmp0);
	mat2_2_2_mult(bstar_r_tmp0, R_theta_l, bstar_r_tmp1);
	bstar_r[0] = bstar_r_tmp1[0][0]; 
	bstar_r[1] = bstar_r_tmp1[1][0]; 
	bstar_r[2] = bstar_r_tmp1[0][1]; 
	bstar_r[3] = bstar_r_tmp1[1][1];

	mat2_2_2_mult(A_T_sr, D_r, bstar_l_tmp0);
	mat2_2_2_mult(bstar_l_tmp0, R_theta_r, bstar_l_tmp1);
	bstar_l[0] = bstar_l_tmp1[0][0]; 
	bstar_l[1] = bstar_l_tmp1[1][0]; 
	bstar_l[2] = bstar_l_tmp1[0][1]; 
	bstar_l[3] = bstar_l_tmp1[1][1];


	mat4_4_4_mult(Astar_r, Astar_l, Tmp_a_4_4);
	mat4_4_sub(Tmp_a_4_4, E44, Astar0_l);
	det_l = mat4_4_determinant(Astar0_l);
	
	mat4_4_4_mult(Astar_l, Astar_r, Tmp_a_4_4);
	mat4_4_sub(Tmp_a_4_4, E44, Astar0_r);
	det_r = mat4_4_determinant(Astar0_r);

	NEAR_ZERO = mwpg.NEAR_ZERO;
	LEFT = mwpg.LEFT;
	RIGHT = mwpg.RIGHT;

	if ( (fabs(det_l) < NEAR_ZERO) || isnan(det_l) || (fabs(det_r) < NEAR_ZERO) || isnan(det_r) ) {
		return -1;
	}
	else {		
		if (supporting_leg == LEFT) {
			mat4_4_1_mult(Astar_r, bstar_l, Tmp_a_4_1);
			mat4_1_add(Tmp_a_4_1, bstar_r, Tmp_b_4_1);
			mat4_4_inv_mult_mat4_1(Astar0_l, Tmp_b_4_1, WS_d);
		}
		else {
			mat4_4_1_mult(Astar_l, bstar_r, Tmp_a_4_1);
			mat4_1_add(Tmp_a_4_1, bstar_l, Tmp_b_4_1);
			mat4_4_inv_mult_mat4_1(Astar0_r, Tmp_b_4_1, WS_d);
		}
	}

	return 0;
}


int compute_desired_initial_WS(
	double CS[10], int supporting_leg, double WS_d[4]) {

	int LEFT, RIGHT;
	double T_c;
	int single_supporting_leg;
	double T_ss;
	double x_offset, y_offset;
	double WS_d_ss[4];
	double C, S;
	double x_f, v_f, y_f, w_f;
	double x_i, v_i, y_i, w_i;
	double x_d, v_d, y_d, w_d;

	LEFT = mwpg.LEFT;
	RIGHT = mwpg.RIGHT;
	T_c = mwpg.T_c;

	if (supporting_leg == RIGHT) {
		single_supporting_leg = LEFT;
		T_ss = CS[0];

		x_offset = 0.0;
		y_offset = mwpg.PELVIS_LENGTH;
	}
	else {
		single_supporting_leg = RIGHT;
		T_ss = CS[1];

		x_offset = 0.0;
		y_offset = -mwpg.PELVIS_LENGTH;
	}

	compute_desired_WS(CS, single_supporting_leg, WS_d_ss);

	C = cosh(-0.45*T_ss/T_c);
	S = sinh(-0.45*T_ss/T_c);
	x_f = WS_d_ss[0];
	v_f = WS_d_ss[1];
	y_f = WS_d_ss[2];
	w_f = WS_d_ss[3];
	x_i = C*x_f + S*v_f;
	v_i = S*x_f + C*v_f;
	y_i = C*y_f + S*w_f;
	w_i = S*y_f + C*w_f;

	x_d = x_offset + x_i;
	v_d = v_i;
	y_d = y_offset + y_i;
	w_d = w_i;

	WS_d[0] = x_d; WS_d[1] = v_d; WS_d[2] = y_d; WS_d[3] = w_d;

	return 0;
}



int compute_F_function(
	double T_ss, double x_k, double v_k, double x_d, double *A, double *alpha) {

	double NEAR_ZERO;
	double d;

	NEAR_ZERO = mwpg.NEAR_ZERO;
	
	if(fabs(T_ss) < NEAR_ZERO) {
		*A = 0.0;
		*alpha = 0.0;
		return 0;
	}

	d = x_d - x_k;

	if (fabs(v_k) < NEAR_ZERO) {
		*alpha = 0.5;
	}
	else if (v_k > 0.0) {
		*alpha = (d - sqrt(d*d - v_k*T_ss*d + 0.5*(v_k*T_ss)*(v_k*T_ss)))/(v_k*T_ss);
	}
	else {
		*alpha = (d + sqrt(d*d - v_k*T_ss*d + 0.5*(v_k*T_ss)*(v_k*T_ss)))/(v_k*T_ss);
	}

	if (fabs(*alpha - 0.5) < NEAR_ZERO ) {
		*A = 4.0*(d - v_k*T_ss)/(T_ss*T_ss);
	}
	else {
		*A = -v_k/(T_ss*(2.0*(*alpha) - 1.0));
	}

	return 0;
}



int compute_FS_ds_motion(double FS_k[6], double F_R_k[2], double CC_k[2],
	double FS_kp1[6], double F_R_kp1[2], double CC_kp1[2]) {

	FS_kp1[0] = FS_k[0];
	FS_kp1[1] = FS_k[1];
	FS_kp1[2] = FS_k[2];
	FS_kp1[3] = FS_k[3];
	FS_kp1[4] = FS_k[4];
	FS_kp1[5] = FS_k[5];

	F_R_kp1[0] = F_R_k[0];
	F_R_kp1[1] = F_R_k[1];

	CC_kp1[0] = CC_k[0];
	CC_kp1[1] = CC_k[1];

	return 0;
}


int compute_FS_ss_motion(double FS_k[6], double F_R_k[2], double CC_k[2],
	double FS_t[4], double F_R_t[2], 
	double T_ss, double time, double FS_kp1[6], double F_R_kp1[2], double CC_kp1[2]) {

	double x_d, y_d;
	double x_k, v_k, y_k, w_k;
	double A_x, alpha_x, A_y, alpha_y;
	double T_sw_x, T_sw_y;
	double x_kp1, v_kp1, y_kp1, w_kp1, z_kp1, u_kp1;
	double q_k, dq_k, q_d;	
	double A_q, alpha_q;
	double T_sw_q;
	double qc_k, dqc_k, qc_d;
	double A_qc, alpha_qc;
	double T_sw_qc;
	
	double q_kp1, dq_kp1;
	double qc_kp1, dqc_kp1;

	x_d = FS_t[0];
	y_d = FS_t[2];

	// x-axis motion
	x_k = FS_k[0];
	v_k = FS_k[1];
	compute_F_function(T_ss, x_k, v_k, x_d, &A_x, &alpha_x);

	T_sw_x = alpha_x*T_ss;
	if  (T_sw_x >= time) {
		x_kp1 = x_k + v_k*time + 0.5*A_x*time*time;
		v_kp1 = v_k + A_x*time;
	}
	else {
		x_kp1 = x_k + v_k*time + A_x*(-T_sw_x*T_sw_x + 2.0*T_sw_x*time - 0.5*time*time);
		v_kp1 = v_k + A_x*(2.0*T_sw_x - time);
	}

	// y-axis motion
	y_k = FS_k[2];
	w_k = FS_k[3];
	compute_F_function(T_ss, y_k, w_k, y_d, &A_y, &alpha_y);

	T_sw_y = alpha_y*T_ss;
	if (T_sw_y >= time) {
		y_kp1 = y_k + w_k*time + 0.5*A_y*time*time;
		w_kp1 = w_k + A_y*time;
	}
	else {
		y_kp1 = y_k + w_k*time + A_y*(-T_sw_y*T_sw_y + 2.0*T_sw_y*time - 0.5*time*time);
		w_kp1 = w_k + A_y*(2.0*T_sw_y - time);
	}

	// rotation_motion (z-axis rotation)
	q_k = F_R_k[0];
	dq_k = F_R_k[1];
	q_d = F_R_t[0];
	compute_F_function(T_ss, q_k, dq_k, q_d, &A_q, &alpha_q);

	T_sw_q = alpha_q*T_ss;
	if (T_sw_q >= time) {
		q_kp1 = q_k + dq_k*time + 0.5*A_q*time*time;
		dq_kp1 = dq_k + A_q*time;
	}
	else {
		q_kp1 = q_k + dq_k*time + A_q*(-T_sw_q*T_sw_q + 2.0*T_sw_q*time - 0.5*time*time);
		dq_kp1 = dq_k + A_q*(2.0*T_sw_q - time);
	}

	// z-axis motion (foot height)
	// 발의 높이는 cycloid motion을 통해서 결정한다.
	qc_k = CC_k[0];
	dqc_k = CC_k[1];
	//qc_d = PI;	// desire angle은 항상 180도 이다.
	qc_d = 2.0*PI;	// desire angle은 항상 360도 이다.
	compute_F_function(T_ss, qc_k, dqc_k, qc_d, &A_qc, &alpha_qc);

	T_sw_qc = alpha_qc*T_ss;
	if (T_sw_qc >= time) {
		qc_kp1 = qc_k + dqc_k*time + 0.5*A_qc*time*time;
		dqc_kp1 = dqc_k + A_qc*time;
	}
	else {
		qc_kp1 = qc_k + dqc_k*time + A_qc*(-T_sw_qc*T_sw_qc + 2.0*T_sw_qc*time - 0.5*time*time);
		dqc_kp1 = dqc_k + A_qc*(2.0*T_sw_qc - time);
	}
	// compute z-axis motion from cycloid motion
#ifdef CYCLOID1
	z_kp1 = mwpg.Rc*(1.0 - cos(qc_kp1));
#endif

#ifdef CYCLOID2	
	z_kp1 = mwpg.Rc*(1.0 - cos(2.0*PI*(0.25 - T_ss)/0.25));
#endif
	u_kp1 = mwpg.Rc*sin(qc_kp1);

	FS_kp1[0] = x_kp1;
	FS_kp1[1] = v_kp1;
	FS_kp1[2] = y_kp1;
	FS_kp1[3] = w_kp1;
	FS_kp1[4] = z_kp1;
	FS_kp1[5] = u_kp1;

	F_R_kp1[0] = q_kp1;
	F_R_kp1[1] = dq_kp1;

	CC_kp1[0] = qc_kp1;
	CC_kp1[1] = dqc_kp1;

	return 0;
}

double compute_minimum_time(
	double a_max, double x_k, double dx_k, double x_d) {

	double NEAR_ZERO;
	double d;
	double alpha, A, T, Ad;
	double t_min;

	NEAR_ZERO = mwpg.NEAR_ZERO;

	d = x_d - x_k;
	if (fabs(d) < NEAR_ZERO) {
		alpha = sqrt(2.0)/2.0;
		if (dx_k > 0.0) {
			A = -fabs(a_max);
		}
		else {
			A = fabs(a_max);
		}
		T = -dx_k/(A*(2.0*alpha - 1.0));
	}
	else if(fabs(dx_k) < NEAR_ZERO) {
		if (d > 0) {
			A = fabs(a_max);
		}
		else {
			A = -fabs(a_max);
		}
		T = sqrt(4.0*d/A);
	}
	else {
		if (d > 0) {
			A = fabs(a_max);
		}
		else {
			A = -fabs(a_max);
		}
		Ad = a_max*d;
		if (dx_k > 0.0) {
			alpha = (4.0*Ad - fabs(dx_k)*sqrt(4.0*Ad + 2.0*dx_k*dx_k))/(2.0*(4.0*Ad + dx_k*dx_k));
		}
		else {
			alpha = (4.0*Ad + fabs(dx_k)*sqrt(4.0*Ad + 2.0*dx_k*dx_k))/(2.0*(4.0*Ad + dx_k*dx_k));
		}

		T = -dx_k/(A*(2.0*alpha - 1.0));
	}

	t_min = T;

	return t_min;
}

double compute_minimum_ss_time(
	double CS[10], double FS_k[4], int supporting_leg) {

	double x_k, y_k, dx_k, dy_k;
	double F_XY_ACC_MAX;
	double x_d, y_d;
	double tx_min, ty_min;
	double t_ss_min;

	x_k = FS_k[0];
	dx_k = FS_k[1];
	y_k = FS_k[2];
	dy_k = FS_k[3];

	F_XY_ACC_MAX = mwpg.F_XY_ACC_MAX;

	if (supporting_leg == mwpg.LEFT) {
		x_d = CS[5];
		y_d = CS[7];
	}
	else {
		x_d = CS[4];
		y_d = CS[6];
	}

	tx_min = compute_minimum_time(F_XY_ACC_MAX, x_k, dx_k, x_d);
	ty_min = compute_minimum_time(F_XY_ACC_MAX, y_k, dy_k, y_d);

	if (tx_min < ty_min) {
		t_ss_min = ty_min;
	}
	else {
		t_ss_min = tx_min;
	}

	return t_ss_min;
}

int convert_WS_wrt_imagine_leg(double WS_sup[4], double FS_swing[6], 
	double WS_img[4]) {

	double x, v, y, w;
	double fx, fy, x_img0, y_img0;
	double x_new, v_new, y_new, w_new;

	x = WS_sup[0];
	v = WS_sup[1];
	y = WS_sup[2];
	w = WS_sup[3];

	fx = FS_swing[0];
	fy = FS_swing[2];
	x_img0 = 0.5*fx;
	y_img0 = 0.5*fy;

	x_new = x - x_img0;
	v_new = v;
	y_new = y - y_img0;
	w_new = w;

	WS_img[0] = x_new; WS_img[1] = v_new; WS_img[2] = y_new; WS_img[3] = w_new;

	return 0;
}

int convert_WS_wrt_supporting_leg(double WS_img[4], 
	double FS_swing[6], double WS_sup[4]) {

	double x, v, y, w;
	double fx, fy, x_img0, y_img0;
	double x_new, v_new, y_new, w_new;

	x = WS_img[0];
	v = WS_img[1];
	y = WS_img[2];
	w = WS_img[3];

	fx = FS_swing[0];
	fy = FS_swing[2];
	x_img0 = 0.5*fx;
	y_img0 = 0.5*fy;

	x_new = x + x_img0;
	v_new = v;
	y_new = y + y_img0;
	w_new = w;

	WS_sup[0] = x_new; WS_sup[1] = v_new; WS_sup[2] = y_new; WS_sup[3] = w_new;

	return 0;
}

int compute_target_WS_FS(WP *w_k, double CS[10], double CS_old[10], 
	double CS_mod[10], 
	WP *w_t, double *T_ss_t, double *P_t, double *T_sw_t, double *Q_t) {
	
	int TRUE, FALSE;
	double P_MAX, P_MIN, Q_MAX, Q_MIN;
	double T_SS_MAX, T_SS_MIN;
	int sagittalFeasibility, lateralFeasibility;
	double WS_t[4], CM_R_t[2], FS_t[4], F_R_t[2];
	double WS_d[4];
	double T_ss_d, P_d, T_sw_d, Q_d;
	double F_l, F_r, S_l, S_r, theta_l, theta_r;
	

	// binary search
	double CS_init[10], CS_final[10], CS_modified[10], CS_mean[10], CS_tmp[10];
	double WS_m[4];
	double T_ss_m, P_m, T_sw_m, Q_m;
	int Is_CS_modified;
	int i;

  
	TRUE = mwpg.TRUE;
	FALSE = mwpg.FALSE;
	P_MAX = mwpg.P_MAX;
	P_MIN = mwpg.P_MIN;
	Q_MAX = mwpg.Q_MAX;
	Q_MIN = mwpg.Q_MIN;
	T_SS_MAX = mwpg.T_SS_MAX;
	T_SS_MIN = compute_minimum_ss_time(CS, w_k->FS, w_k->sup_leg);

	compute_desired_WS(CS, w_k->sup_leg, WS_d);

	check_CM_feasibility(w_k->WS, WS_d, T_SS_MAX, T_SS_MIN, mwpg.PLUS_TOLERANCE, &sagittalFeasibility, &lateralFeasibility, &T_ss_d, &P_d, &T_sw_d, &Q_d);

#ifdef BIANRY
	if ( (sagittalFeasibility == TRUE) && (lateralFeasibility == TRUE) ) {
		mat4_1_copy(WS_d, WS_t);
		*T_ss_t = T_ss_d;
		*P_t = P_d;
		*T_sw_t = T_sw_d;
		*Q_t = Q_d;
		mat10_1_copy(CS, CS_modified);
	}
	else {

		mat10_1_copy(CS_old, CS_init);
		mat10_1_copy(CS, CS_final);
		mat10_1_copy(CS_init, CS_modified);
		Is_CS_modified = 0;

		// 초기값 계산
		// 과거의 CS 기준으로 desired WS 계산
		compute_desired_WS(CS_init, w_k->sup_leg, WS_m);
		T_SS_MIN = compute_minimum_ss_time(CS_init, w_k->FS, w_k->sup_leg);
		check_CM_feasibility(w_k->WS, WS_m, T_SS_MAX, T_SS_MIN, 
			mwpg.PLUS_TOLERANCE, &sagittalFeasibility, &lateralFeasibility,
			&T_ss_d, &P_d, &T_sw_d, &Q_d);
		if( (sagittalFeasibility == FALSE) || (lateralFeasibility == FALSE) ) {
			printf("error\n");
		}
		//T_SS_MIN = compute_minimum_ss_time(mwpg, CS_init, FS_k, supporting_leg);
		for(i=0; i<N_ITER_BINARY_SEARCH; i++) {
			// CS 평균값 계산
			mat10_1_add(CS_init, CS_final, CS_tmp);
			mat10_1_scale(CS_tmp, 0.5, CS_mean);
			// feasibility 체크
			compute_desired_WS(CS_mean, w_k->sup_leg, WS_d);
			T_SS_MIN = compute_minimum_ss_time(CS_mean, w_k->FS, w_k->sup_leg);
			check_CM_feasibility(w_k->WS, WS_d, T_SS_MAX, T_SS_MIN, 
				mwpg.MINUS_TOLERANCE, &sagittalFeasibility, &lateralFeasibility,
				&T_ss_d, &P_d, &T_sw_d, &Q_d);
			if( (sagittalFeasibility == TRUE) && (lateralFeasibility == TRUE) ) {
				// update
				mat10_1_copy(CS_mean, CS_init);
				mat10_1_copy(CS_mean, CS_modified);
				T_ss_m = T_ss_d;
				P_m = P_d;
				T_sw_m = T_sw_d;
				Q_m = Q_d;
				mat4_1_copy(WS_d, WS_m);
				Is_CS_modified = 1;
			}
			else {
				mat10_1_copy(CS_mean, CS_final);
			}
		}
		mat4_1_copy(WS_m, WS_t);
		*T_ss_t = T_ss_m;
		*P_t = P_m;
		*T_sw_t = T_sw_m;
		*Q_t = Q_m;
		//mat10_1_copy(CS_modified, CS_modified);
	}
#endif

#ifndef BIANRY
		mat4_1_copy(WS_d, WS_t);
		*T_ss_t = T_ss_d;
		*P_t = P_d;
		*T_sw_t = T_sw_d;
		*Q_t = Q_d;
		mat10_1_copy(CS, CS_modified);
#endif
 
  
  
  
	F_l = CS_modified[4];
	F_r = CS_modified[5];
	S_l = CS_modified[6];
	S_r = CS_modified[7];

	//S_l = CS[6];
	//S_r = CS[7];
  
	theta_l = CS_modified[8];
	theta_r = CS_modified[9];

//cout << F_l << "\t" << F_r  << endl;
  

	if (w_k->sup_leg == mwpg.LEFT) {
		FS_t[0] = F_r;
		FS_t[1] = 0.0;
		FS_t[2] = S_r;
		FS_t[3] = 0.0;
		F_R_t[0] = theta_r;
		F_R_t[1] = 0.0;
	}
	else {
		FS_t[0] = F_l;
		FS_t[1] = 0.0;
		FS_t[2] = S_l;
		FS_t[3] = 0.0;
		F_R_t[0] = theta_l;
		F_R_t[1] = 0.0;
	}

	CM_R_t[0] = 0.5*FS_t[0];
	CM_R_t[1] = 0.0;

	// 조립
	mat4_1_copy(WS_t, w_t->WS);
	mat2_1_copy(CM_R_t, w_t->CM_R);
	mat4_1_copy(FS_t, w_t->FS);
	mat2_1_copy(F_R_t, w_t->F_R);

	mat10_1_copy(CS_modified, CS_mod);

	/*
	if(isnan(*T_ss_t)) {
		printf("CCC\n");
	}
	*/

	//printf("iter of bin: %d\t Is_CS_modified=%d\n", i, Is_CS_modified);	
	//printf("CS=%f  %f  %f  %f  %f  %f  %f  %f  %f  %f\n", CS_mod[0],CS_mod[1],CS_mod[2],CS_mod[3],CS_mod[4],CS_mod[5],CS_mod[6],CS_mod[7],CS_mod[8],CS_mod[9]);
	//printf("T_SS_MIN=%f\t, T_ss_t=%f\t, P_t=%f\t, T_sw_t=%f\t, Q_t=%f\n", T_SS_MIN, *T_ss_t, *P_t, *T_sw_t, *Q_t);
	return 0;
}

int compute_CM_ds_start_stop_motion(double WS_k[4], double FS_k[6], 
	double WS_d[4],	double T_ds_remain, double t_ds, double WS_kp1[4]) {

	double WS_img[4], WS_d_img[4];
	double x_k_img, v_k_img, y_k_img, w_k_img;
	double x_d_img, v_d_img, y_d_img, w_d_img;
	double T_sw_x, Q_x;
	double T_sw_y, Q_y;
	double x_kp1_img, v_kp1_img;
	double y_kp1_img, w_kp1_img;
	double WS_kp1_img[4];

	// 양발 사이의 가상의 supporting leg 로 좌표 변환
	convert_WS_wrt_imagine_leg(WS_k, FS_k, WS_img);
	convert_WS_wrt_imagine_leg(WS_d, FS_k, WS_d_img);

	x_k_img = WS_img[0];
	v_k_img = WS_img[1];
	y_k_img = WS_img[2];
	w_k_img = WS_img[3];
	x_d_img = WS_d_img[0];
	v_d_img = WS_d_img[1];
	y_d_img = WS_d_img[2];
	w_d_img = WS_d_img[3];

	// Q function 계산
	compute_Q_function(x_k_img, v_k_img, x_d_img, v_d_img, T_ds_remain, &T_sw_x, &Q_x);
	compute_Q_function(y_k_img, w_k_img, y_d_img, w_d_img, T_ds_remain, &T_sw_y, &Q_y);

	compute_CM_motion_with_Q_function(T_sw_x, Q_x, x_k_img, v_k_img, t_ds, &x_kp1_img, &v_kp1_img);
	compute_CM_motion_with_Q_function(T_sw_y, Q_y, y_k_img, w_k_img, t_ds, &y_kp1_img, &w_kp1_img);

	WS_kp1_img[0] = x_kp1_img;
	WS_kp1_img[1] = v_kp1_img;
	WS_kp1_img[2] = y_kp1_img;
	WS_kp1_img[3] = w_kp1_img;

	// imagine leg 좌표계에서 다시 supporting leg 로 변환
	convert_WS_wrt_supporting_leg(WS_kp1_img, FS_k, WS_kp1);

	return 0;
}

int compute_ds_start_stop_motion(WP *wp_k, double WS_d[4], WP *wp_kp1, 
	double t_ds, int state){

	//dbg(777);
	compute_CM_ds_start_stop_motion(wp_k->WS, wp_k->FS, WS_d, wp_k->T_ds_remain, 
		t_ds, wp_kp1->WS);
	compute_FS_ds_motion(wp_k->FS, wp_k->F_R, wp_k->CC, wp_kp1->FS, wp_kp1->F_R, wp_kp1->CC);
	mat2_1_scale(wp_kp1->F_R, 0.5, wp_kp1->CM_R);
	wp_kp1->sup_leg = wp_k->sup_leg;
	wp_kp1->state = state;
	wp_kp1->T_ds_remain = wp_k->T_ds_remain - t_ds;

	return 0;
}


int compute_ss_motion(
	double T_ss, double P, double T_sw, double Q, 
	WP *wp_k, WP *wp_t, double t_ss, WP *wp_kp1) {
	
	compute_CM_ss_motion(T_ss, P, T_sw, Q, wp_k->WS, t_ss, wp_kp1->WS);
	compute_FS_ss_motion(wp_k->FS, wp_k->F_R, wp_k->CC, wp_t->FS, wp_t->F_R, T_ss, t_ss, 
		wp_kp1->FS, wp_kp1->F_R, wp_kp1->CC);
	mat2_1_scale(wp_kp1->F_R, 0.5, wp_kp1->CM_R);
	wp_kp1->sup_leg = wp_k->sup_leg;
	wp_kp1->state = mwpg.SS;
	wp_kp1->T_ds_remain = 0.0;

	return 0;
}


int compute_ds_motion(WP *wp_k, WP *wp_kp1, double t_ds) {

	compute_CM_ds_motion(wp_k->WS, t_ds, wp_kp1->WS);
	compute_FS_ds_motion(wp_k->FS, wp_k->F_R, wp_k->CC, wp_kp1->FS, wp_kp1->F_R, wp_kp1->CC);
	mat2_1_scale(wp_kp1->F_R, 0.5, wp_kp1->CM_R);
	wp_kp1->sup_leg = wp_k->sup_leg;
	wp_kp1->state = mwpg.DS;
	wp_kp1->T_ds_remain = wp_k->T_ds_remain - t_ds;

	return 0;
}

int compute_reference_CM_foot_motion(
	double CS[10], double CS_old[10], 
	WP *wp_k, int stop_flag, WP *wp_kp1, double CS_mod[10]) {

	double T_sample;
	int DS_START, SS, DS, DS_STOP, STOP;
	int LEFT, RIGHT;
	double WS_d[4];
	double t_ds, t_ss;
	double T_ss=0.0, P=0.0, T_sw=0.0, Q=0.0;
	WP wp_ds, wp_mid, wp_t;

	int TRUE, FALSE;

	T_sample = mwpg.T_samples;

	DS_START = mwpg.DS_START;
	SS = mwpg.SS;
	DS = mwpg.DS;
	DS_STOP = mwpg.DS_STOP;
	STOP = mwpg.STOP;

	LEFT = mwpg.LEFT;
	RIGHT = mwpg.RIGHT;

	TRUE = mwpg.TRUE;
	FALSE = mwpg.FALSE;

	//dbg(wp_k->state);
	if (wp_k->state == DS_START) {
		mat10_1_copy(CS, CS_mod);

		compute_desired_initial_WS(CS, wp_k->sup_leg, WS_d);
		//dbg(000);
		if (wp_k->T_ds_remain >= T_sample) {
			t_ds = T_sample;
			compute_ds_start_stop_motion(wp_k, WS_d, wp_kp1, t_ds, DS_START);
			//dbg(111);
		}
		else {			
			// ds motion
			t_ds = wp_k->T_ds_remain;
			
			compute_ds_start_stop_motion(wp_k, WS_d, &wp_ds, t_ds, DS_START);
			//dbg(999);
			// change supporting leg
			change_supporting_leg(&wp_ds, &wp_mid);
			wp_mid.T_ds_remain = 0.0;
			
			// ss motion			
			compute_target_WS_FS(&wp_mid, CS, CS_old, CS_mod, 
				&wp_t, &T_ss, &P, &T_sw, &Q);			
			t_ss = T_sample - t_ds;
			compute_ss_motion(T_ss, P, T_sw, Q, &wp_mid, &wp_t, t_ss, wp_kp1);
			//printf("t_ss: %f\n", t_ss);
			//printf("T_ss: %f\n", T_ss);
			//dbg(222);			
		}
 
	}
	else if (wp_k->state == SS) {
		compute_target_WS_FS(wp_k, CS, CS_old, CS_mod, &wp_t,
			&T_ss, &P, &T_sw, &Q);

		if(CS[0] < mwpg.NEAR_ZERO){
			dbg(124);
		}
		//dbg(444);
		
		if(isnan(T_ss)) {
			printf("BBB\n");
		}

		if (T_ss >= T_sample) {
			t_ss = T_sample;
			compute_ss_motion(T_ss, P, T_sw, Q, wp_k, &wp_t, t_ss, wp_kp1);			
		}
		else {
			// ss motion
			t_ss = T_ss;
			compute_ss_motion(T_ss, P, T_sw, Q, wp_k, &wp_t, t_ss, &wp_mid);

			// ds motion
			t_ds = T_sample - T_ss;			
			if(stop_flag == TRUE) {
				wp_mid.T_ds_remain = mwpg.T_ds_stop;	// 인위적으로 설정
				mat4_1_scale(wp_k->FS, 0.5, WS_d);
				compute_ds_start_stop_motion(&wp_mid, WS_d, wp_kp1, t_ds, DS_STOP);
			}
			else {
				if (wp_mid.sup_leg == LEFT) {
					wp_mid.T_ds_remain = CS_mod[2];
				}
				else {
					wp_mid.T_ds_remain = CS_mod[3];
				}

				compute_ds_motion(&wp_mid, wp_kp1, t_ds);				
			}
		}

	}
	else if (wp_k->state == DS) {		
    
		//dbg(555);
		if (wp_k->T_ds_remain >= T_sample) {
			t_ds = T_sample;
			compute_ds_motion(wp_k, wp_kp1, t_ds);
		}
		else {			
			// ds motion
			t_ds = wp_k->T_ds_remain;
			compute_ds_motion(wp_k, &wp_ds, t_ds);						

			// ss motion
			/*
			// single support 에 들어가기 전에 CS 업데이트
			mat10_1_copy(CS, CS_mod);
			*/
			if(CS[0] < mwpg.NEAR_ZERO) {
				dbg(654);
			}

			change_supporting_leg(&wp_ds, &wp_mid);
			wp_mid.T_ds_remain = 0.0;

			t_ss = T_sample - t_ds;
			compute_target_WS_FS(&wp_mid, CS, CS_old, CS_mod, &wp_t,
				&T_ss, &P, &T_sw, &Q);
			
			compute_ss_motion(T_ss, P, T_sw, Q, &wp_mid, &wp_t, t_ss, wp_kp1);
			if(CS[0] < mwpg.NEAR_ZERO) {
				dbg(654);
			}
		}
	}
	else if (wp_k->state == DS_STOP) {
		//dbg(666);
		//mat10_1_copy(CS, CS_mod);
		mat4_1_scale(wp_k->FS, 0.5, WS_d);
		if (wp_k->T_ds_remain >= T_sample) {
			t_ds = T_sample;
			compute_ds_start_stop_motion(wp_k, WS_d, wp_kp1, t_ds, DS_STOP);
		}
		else {
			t_ds = wp_k->T_ds_remain;
			compute_ds_start_stop_motion(wp_k, WS_d, wp_kp1, t_ds, STOP);
			wp_kp1->T_ds_remain = 0.0;
		}
      
      
	}
	else {
		wp_copy(wp_k, wp_kp1);
		//dbg(333);
	}

	//dbg(888);
	//printf("T_ss=%f\t, P=%f\t, T_sw=%f\t, Q=%f\n", T_ss, P, T_sw, Q);
	if(CS[0] < mwpg.NEAR_ZERO) {
		dbg(1435);
	}

	if(wp_kp1->WS[1] > 100) {
		printf("BBB\n");
	}
	if(isnan(wp_kp1->FS[0])) {
		printf("AAA\n");
	}
  

  global_P = P;    
  global_Q = Q;
  

	return 0;
}

int convert_state_wrt_global(double WS_local[4], double CM_R_local[2], double FS_local[4], double F_R_local[2], double Offset[3],
	double WS_global[4], double CM_R_global[2], double FS_global[4], double F_R_global[2]) {

	double x_offset, y_offset, q_offset;
	double c, s;
	double x_local, v_local, y_local, w_local;
	double x_global, v_global, y_global, w_global;
	double q_local, dq_local;
	double q_global, dq_global;
	double fx_local, fv_local, fy_local, fw_local;
	double fx_global, fv_global, fy_global, fw_global;
	double fq_local, fdq_local;
	double fq_global, fdq_global;

	x_offset = Offset[0];
	y_offset = Offset[1];
	q_offset = Offset[2];

	c = cos(q_offset);
	s = sin(q_offset);

	x_local = WS_local[0];
	v_local = WS_local[1];
	y_local = WS_local[2];
	w_local = WS_local[3];

	x_global = x_offset + c*x_local - s*y_local;
	y_global = y_offset + s*x_local + c*y_local;

	v_global = c*v_local - s*w_local;
	w_global = s*v_local + c*w_local;

	q_local = CM_R_local[0];
	dq_local = CM_R_local[1];

	q_global = q_offset + q_local;
	dq_global = dq_local;

	fx_local = FS_local[0];
	fv_local = FS_local[1];
	fy_local = FS_local[2];
	fw_local = FS_local[3];



	fx_global = x_offset + c*fx_local - s*fy_local;
	fy_global = y_offset + s*fx_local + c*fy_local;

	fv_global = c*fv_local - s*fw_local;
	fw_global = s*fv_local + c*fw_local;

	fq_local = F_R_local[0];
	fdq_local = F_R_local[1];

	fq_global = q_offset + fq_local;
	fdq_global = fdq_local;

	WS_global[0] = x_global;
	WS_global[1] = v_global;
	WS_global[2] = y_global;
	WS_global[3] = w_global;

	CM_R_global[0] = q_global;
	CM_R_global[1] = dq_global;

	FS_global[0] = fx_global;
	FS_global[1] = fv_global;
	FS_global[2] = fy_global;
	FS_global[3] = fw_global;

	F_R_global[0] = fq_global;
	F_R_global[1] = fdq_global;

	return 0;
}
//////////////////////////////////////////////////////////////////////////////




#define DEVICE_NAME     "dpram"      // RTDM�뿉 �벑濡앸맂 �뵒諛붿씠�뒪 �씠由� 


// For xenomai
RT_TASK rt_task_main;               // RT task descriptor (rt-task�뿉 愿��븳 �뜲�씠�꽣媛� ����옣�릺�뒗 蹂��닔)
RT_TASK rt_task_controller;         // RT task descriptor (rt-task�뿉 愿��븳 �뜲�씠�꽣媛� ����옣�릺�뒗 蹂��닔)
RT_TASK rt_task_dpram;              // RT task descriptor (rt-task�뿉 愿��븳 �뜲�씠�꽣媛� ����옣�릺�뒗 蹂��닔)
RT_TASK rt_task_imu;                // RT task descriptor (rt-task�뿉 愿��븳 �뜲�씠�꽣媛� ����옣�릺�뒗 蹂��닔)
RT_TASK rt_task_hokuyo;             // RT task descriptor (rt-task�뿉 愿��븳 �뜲�씠�꽣媛� ����옣�릺�뒗 蹂��닔)


// Define mutex
std::mutex g_mutex_for_controller;
std::mutex g_mutex_for_imu;
std::mutex g_mutex_for_hokuyo;
std::mutex g_mutex_for_quit;



// Define quit variable
bool g_is_quit = false;


// Define variables for IMU
boost::asio::serial_port* g_serial_for_imu;
unsigned char g_recv_buffer_for_imu[1024];
std::vector<unsigned char> g_recv_stacked_buffer_for_imu;


// �젅�씠����뒪罹� �룷�씤�듃 媛�
Eigen::MatrixXd g_scanned_points;

// IMU�꽱�꽌濡쒕���꽣 �뼸��� quaternion 媛�
Eigen::Quaterniond g_imu_quat;


// �젣�뼱 蹂��닔
state_data   g_sdata;               // state_data 援ъ“泥� (dpram_dev.h�뿉�꽌 �젙�쓽)
control_data g_cdata;             // control_data 援ъ“泥� (dpram_dev.h�뿉�꽌 �젙�쓽)


// Setting data from sensors
void set_imu_quat(const Eigen::Quaterniond& quat);
void set_scanned_points(const Eigen::MatrixXd& scanned_points);
void set_robot_state_data(const state_data& sdata);
void set_robot_control_data(const control_data& cdata);

// Getting data obtained from sensors
Eigen::Quaterniond get_imu_quat( );
Eigen::MatrixXd get_scanned_points( );
state_data get_robot_state_data( );
control_data get_robot_control_data( );

// Quit checking function
bool is_quit() {
    std::lock_guard<std::mutex> lock(g_mutex_for_quit);
    return g_is_quit;
}

// Do quit
void quit() {
    std::lock_guard<std::mutex> lock(g_mutex_for_quit);
    g_is_quit = true;
}


// IMU �꽱�꽌濡쒕���꽣 �뜲�씠�꽣瑜� 諛쏅뒗 �빖�뱾�윭
void handler_imu(const boost::system::error_code& error, size_t bytes_transferred) {
    if(!error) {
        g_mutex_for_imu.lock();

        // Reserves space to save received packets
        g_recv_stacked_buffer_for_imu.reserve(g_recv_stacked_buffer_for_imu.size()+bytes_transferred);

        // Saves received packets
        for(std::size_t i = 0; i < bytes_transferred; ++i) {
            g_recv_stacked_buffer_for_imu.push_back(g_recv_buffer_for_imu[i]);
        }
        g_mutex_for_imu.unlock();

        // Restarts async read function
        g_serial_for_imu->async_read_some(
            boost::asio::buffer(g_recv_buffer_for_imu,1024),
            handler_imu);
    }
    else {
        if(!is_quit()){
            std::cout<< "error on read " << std::endl;
        }
    }
}

// IMU濡쒕���꽣 �뜲�씠�꽣瑜� 二쇨린�쟻�쑝濡� �빐�꽍�븯�뒗 �벐�젅�뱶
void run_imu(void*) {
    unsigned long control_ns = 10*1000*1000;
    RTIME now, previous;

    // Setting current task as a periodic task
    rt_task_set_periodic(&rt_task_imu, TM_NOW, control_ns);

    while (!is_quit()) {
        previous = rt_timer_read();

        // Gets stacked packets
        g_mutex_for_imu.lock();
        std::vector<unsigned char> copy_stackted_buffer = g_recv_stacked_buffer_for_imu;
        g_recv_stacked_buffer_for_imu.clear();
        g_mutex_for_imu.unlock();


        // Splits packets to get last received packet
        std::vector<std::string> splited_packets;
        boost::split(splited_packets, copy_stackted_buffer, boost::is_any_of("*"));

        if(splited_packets.size() % 2 == 0) {
            // Splits packets again to know end of a packet
            std::vector<std::string> splited_packets2;
            boost::algorithm::split_regex(
                splited_packets2,
                splited_packets.back(),
                boost::regex("\r\n"));

            if(splited_packets2.size() == 2) {

                // Splits packets again to convert quaternion values
                std::vector<std::string> splited_packets3;
                boost::split(splited_packets3, splited_packets2.front(), boost::is_any_of(","));

                // Only receiving a quaternion value
                if(splited_packets3.size() == 4) {
                    try {
                        // Gets quaternion value
                        Eigen::Quaterniond quat;
                        /* 
			// for EBIMU_6DOFV2
			quat.x() = boost::lexical_cast<double>(splited_packets3[0]);
                        quat.y() = boost::lexical_cast<double>(splited_packets3[1]);
                        quat.z() = boost::lexical_cast<double>(splited_packets3[2]);
                        quat.w() = boost::lexical_cast<double>(splited_packets3[3]);
			*/
			
			// for EBIMU_9DOFV4
                        quat.z() = boost::lexical_cast<double>(splited_packets3[0]);
                        quat.y() = boost::lexical_cast<double>(splited_packets3[1]);
                        quat.x() = boost::lexical_cast<double>(splited_packets3[2]);
                        quat.w() = boost::lexical_cast<double>(splited_packets3[3]);
                        // Setting global variable
                        set_imu_quat(quat);
                    }
                    catch(boost::bad_lexical_cast &)
                    {
                    }
                }
            }
        }

        // Waits control time
        rt_task_wait_period(NULL);

        // Prints contol time
        now = rt_timer_read();
        if(std::abs((double)(now - previous) - control_ns) > control_ns*0.01) {
            //printf("IMU: Time since last turn: %ld.%06ld ms\n",
            //       (long)(now - previous) / 1000000,
            //       (long)(now - previous) % 1000000);
        }
    }
    printf("=== END IMU ===\n");
    fflush(stdout);

}

// �젅�씠����꽱�꽌濡쒕���꽣 �뜲�씠�꽣瑜� 二쇨린�쟻�쑝濡� �빐�꽍�븯�뒗 �벐�젅�뱶
void run_hokuyo(void*) {

    unsigned long control_ns = 100*1000*1000;
    RTIME now, previous;


    std::string port_options("type=serial,device=/dev/ttyACM0,timeout=1");

    hokuyoaist::Sensor hokuyo; // Hokuyo scanner object
    hokuyo.set_verbose(false);


    // Open the hokuyo
    hokuyo.open(port_options);

    // Calibrate the hokuyo time stamp
    std::cout << "Calibrating hokuyo time\n";
    hokuyo.calibrate_time();
    std::cout << "1" << std::endl;
    std::cout << "Calculated offset: " << hokuyo.time_offset() << "ns\n";
    std::cout << "Calculated drift rate: " << hokuyo.drift_rate() << '\n';
    std::cout << "Calculated skew alpha: " << hokuyo.skew_alpha() << '\n';

    // Turn the hokuyo on
    hokuyo.set_power(true);
    try
    {
        hokuyo.set_motor_speed(0);
    }
    catch(hokuyoaist::MotorSpeedError &e)
    {
        std::cerr << "Failed to set motor speed: " << e.what() << '\n';
    }
    catch(hokuyoaist::ResponseError &e)
    {
        std::cerr << "Failed to set motor speed: " << e.what() << '\n';
    }

    // Gets sensor information 
    hokuyoaist::SensorInfo info;
    hokuyo.get_sensor_info(info);
    std::cout << info.as_string();


    // Sets scan angles for start and end time (center is zero rad)
    const double start_angle = -90.0*M_PI/180.0;
    const double end_angle   = 90.0*M_PI/180.0;

    // Gets scannning resolution in radian
    const double resolution = info.resolution;  // radian

    unsigned int cluster_count = 1;

    //int count  = 0;


    // Setting current task as a periodic task
    rt_task_set_periodic(&rt_task_hokuyo, TM_NOW, control_ns);


    // Control loop
    while (!is_quit()) {
        previous = rt_timer_read();

        // Gets range data from Hokuyo sensor
        hokuyoaist::ScanData data;
        hokuyo.get_new_ranges_by_angle(data, start_angle, end_angle, cluster_count);

        // Converting scanned data to 2D points
        Eigen::MatrixXd scanned_points = Eigen::MatrixXd::Zero(2, data.ranges_length());
        for(std::size_t step = 0; step < data.ranges_length(); ++step) {

            double d = data.ranges()[step];
            if(d > 20.0) {
                double theta = start_angle + resolution*step;
                //scanned_points(0, step) = d*std::cos(theta);
                //scanned_points(1, step) = d*std::sin(theta);

		scanned_points(0, step) = d;
   // unit: mm
                scanned_points(1, step) = theta; // unit: rad
            }
            else {
                // When scanned is not valid
                scanned_points(0, step) = 0;
                scanned_points(1, step) = 0;
            }
        }

        // Setting global variable for scanned points
        set_scanned_points(scanned_points);

        /*
        // Saves scanned points as a file
        if(count++ % 100 == 0){
            std::ofstream file("scanned_points.txt", std::ofstream::trunc);

            for(int step = 0; step < scanned_points.cols(); ++step){
                file << scanned_points(0, step) << "\t" << scanned_points(1, step) << std::endl;
            }
            file.close();
        }
        */

        // Waits control time
        rt_task_wait_period(NULL);
         
        // Prints control time
        now = rt_timer_read();
        if(std::abs((double)(now - previous) - control_ns) > control_ns*0.01) {
           // printf("HOKUYO: Time since last turn: %ld.%06ld ms\n",
           //        (long)(now - previous) / 1000000,
           //        (long)(now - previous) % 1000000);
        }

    }

    // Close
    hokuyo.close();
    printf("=== END HOKUYO ===\n");
    fflush(stdout);
}


int print_count = 0;

int CONTROL_START = 0;

// 二쇨린�쟻�쑝濡� 紐⑦꽣媛믪쓣 怨꾩궛�븯�뒗 �벐�젅�뱶
void run_controller(void*) {
	
	////////for MWPG /////////
	REF ref1;
	double q_init[12];
	int CurState = 0;

	if (CurState == 0)
	{
	  ready_position(q_init, &ref1);
	}
	//////////////////////////


    unsigned long control_ns = 1*1000*1000;
    RTIME now, previous;

    state_data sdata;               // state_data 援ъ“泥� (dpram_dev.h�뿉�꽌 �젙�쓽)
    control_data cdata;             // control_data 援ъ“泥� (dpram_dev.h�뿉�꽌 �젙�쓽)

    ///////////////////////////////////////////////////////////////////////////////////
    // Open RTDM device file
    //int device = rt_dev_open(DEVICE_NAME, O_RDWR);            // Device瑜� open()
    //if (device < 0) {
    //    printf("ERROR : can't open device %s (%s)\n", DEVICE_NAME, strerror(-device));
    //    exit(1);
    //}
    /////////////////////////////////////////////////////////////////////////////////

    // Setting current task as a periodic task
    rt_task_set_periodic(&rt_task_controller, TM_NOW, control_ns);
    while (!is_quit()) {

        previous = rt_timer_read();


        // Get quaternion data from IMU sensor
        Eigen::Quaterniond quat = get_imu_quat();

        // Get Scanned points data [x(front), y(left)]^T
        Eigen::MatrixXd scanned_points = get_scanned_points();

        // Get current robot state data (g_sdata -> sdata)
        sdata = get_robot_state_data();


        // Converts quaternion data to 3x3 matrix
        //    It is needed to equalize axis with hokuyo scanner
        Eigen::Matrix3d rotation_mat = quat.matrix();
	//Eigen::Vector3d euler = rotation_mat.eulerAngles(0,1,2);
        
	double beta, alpha, gamma;

	beta = atan2(-rotation_mat(2,0), sqrt(rotation_mat(0,0)*rotation_mat(0,0) + rotation_mat(1,0)*rotation_mat(1,0)) );
	
	if ( (beta > -PI/2.) && (beta < PI/2) )
	{
		alpha = atan2(rotation_mat(1,0)/cos(beta), rotation_mat(0,0)/cos(beta));
		gamma = atan2(rotation_mat(2,1)/cos(beta), rotation_mat(2,2)/cos(beta));
        }
	else if (beta == PI/2.)
	{
		alpha = 0.0;		
		gamma = atan2(rotation_mat(2,1), rotation_mat(2,2));
	}
	else if (beta == -PI/2.)
	{
		alpha = 0.0;		
		gamma = -atan2(rotation_mat(2,1), rotation_mat(2,2));
	}


	// Print rotation matrix
        //std::cout << "Current rotation matrix: \n" << rotation_mat << std::endl << std::endl;


		// Print scanned points	
		int i_pole = 0;
		double pole_distance = 1000;
   // set pole_distance to save scanned points [mm]
		double pole_d[721] = {};
		double pole_theta[721] = {};
		double pole_d_sum = 0.0;
		double pole_theta_sum = 0.0;
		double pole_d_mean = 0.0;
		double pole_theta_mean = 0.0;

		// Print scanned points	
		for(int step = 0; step < scanned_points.cols(); ++step){
			
			if (scanned_points(0, step) < pole_distance )   
			{

				//std::cout << scanned_points(0, step) << std::endl << std::endl;
				pole_d[i_pole] = scanned_points(0, step);
				pole_theta[i_pole] = scanned_points(1, step);
				i_pole = i_pole + 1;
			}
			
		}
		//printf("i_pole: %.2d", i_pole);
		
		
		for (int i=0; i < i_pole; i++)
		{
			pole_d_sum = pole_d_sum + pole_d[i];
			pole_theta_sum = pole_theta_sum + pole_theta[i];
		}
		//printf("i_pole: %.2d\t pole_d_sum: %.2f\t pole_theta_sum: %.2f \n", i_pole, pole_d_sum, pole_theta_sum);

		if (i_pole > 0)
		{
			pole_d_mean = pole_d_sum / i_pole;
			pole_theta_mean = pole_theta_sum / i_pole;
			pole_theta_mean = pole_theta_mean * 180. / PI;
			pole_theta_mean = (int)pole_theta_mean % 360;
		}
		else
		{
			pole_d_mean = 0.0;
			pole_theta_mean = 0.0;
		}		
		
		



	//std::cout << "Current mean scanned point: \n" ;
	
	print_count = print_count + 1;

	if (print_count%10 == 0)
	{
        	//std::cout << "Current rotation matrix: \n" << rotation_mat << std::endl << std::endl;
		//printf("pitch: %.2f\t roll: %.2f\t yaw: %.2f \n", euler[1]*180./PI, euler[0]*180./PI, euler[2]*180./PI);
		//printf("beta(pitch): %.2f\t gamma(roll): %.2f\n", beta*180./PI, gamma*180./PI);
		//printf("i_pole: %.2d\t pole_d_mean: %.2f\t pole_theta_mean: %.2f \n", i_pole, pole_d_mean, pole_theta_mean);
		
		//printf("pitch(deg): %.2f\t roll(deg): %.2f\t pole_d_mean(cm): %.2f\t pole_th_mean(deg): %.2f\n", beta*180./PI, gamma*180./PI, pole_d_mean/10., pole_theta_mean);
		print_count = 0;	
	}
		
        //std::cout << "Current scanned points: \n" << scanned_points.cols()  << std::endl << std::endl;
        
	
	/*
	// Print joint state data
        std::cout << "Current joint state: \n" ;
        for(int i = 0; i < CONTROL_JOINT_NUM; ++i) {
          printf("%.4f %.4f %.4f \n", sdata.torque[i], sdata.velocity[i], sdata.position[i]);
        }
        std::cout << std::endl;
        */

		
        // Make busy for controller
        /*double x = 0.1;
        double y = 0.0;
        for(int i = 0; i < 5000; ++i){
            y = exp(x);
            x += 0.1;
        }
		*/


		////////for MWPG /////////
		if (CurState == 1)
          walking_pattern(&ref1); 
        
		
		
		// Set control data by MWPG
        //std::cout << " set control data: \n" ;
        
		if (CONTROL_START == 0)
		{
			for(int i = 0; i < CONTROL_JOINT_NUM; ++i) {
				cdata.torque[i] = 0.0;
				cdata.velocity[i] = 0.0;
				cdata.position[i] = 0.0;
				//printf("%.4f %.4f %.4f \n", cdata.torque[i], cdata.velocity[i], cdata.position[i]);
			}
		}
		else if (CONTROL_START == 1)
		{
			cdata.position[0] = 10.0;
		}

		/*
        cdata.position[0] = ref1.hipy_q[0];
        cdata.position[1] = GAIN_roll*ref1.hipx_q[0];          
        cdata.position[2] = ref1.hipz_q[0];
        cdata.position[3] = ref1.kneey_q[0];
        cdata.position[4] = ref1.ankley_q[0];                              
        cdata.position[5] = GAIN_roll*ref1.anklex_q[0];		
                      
        cdata.position[6] = ref1.hipy_q[1];
        cdata.position[7] = GAIN_roll*ref1.hipx_q[1];          
        cdata.position[8] = ref1.hipz_q[1];
        cdata.position[9] = ref1.kneey_q[1];
        cdata.position[10] = ref1.ankley_q[1];                              
        cdata.position[11] = GAIN_roll*ref1.anklex_q[1];
		*/
		for(int i = 0; i < 12; ++i) {
			//printf("%.4f\t", cdata.position[i]*180./PI);
			//printf("\n");
		}

		if (CurState == 0 )
        {
           //GaitGenerator::wait(1000);
           //CurState = 1;
        }
		//////////////////////////


	/*
        // Set control data 
        std::cout << " set control data: \n" ;
        for(int i = 0; i < CONTROL_JOINT_NUM; ++i) {
            cdata.torque[i] = rand();
            cdata.velocity[i] = rand();
            cdata.position[i] = rand();
            printf("%.4f %.4f %.4f \n", cdata.torque[i], cdata.velocity[i], cdata.position[i]);
        }
	*/	

        // Get global variable for control data
        set_robot_control_data(cdata);

        //rt_dev_ioctl(device, DPRAM_SEND_CONTROL_DATA, &cdata);

        // Waits control time
        rt_task_wait_period(NULL);

        // Prints control time
        now = rt_timer_read();
        if(std::abs((double)(now - previous) - control_ns) > control_ns*0.1) {
            //printf("CONTROLLER: Time since last turn: %ld.%06ld ms\n",
            //       (long)(now - previous) / 1000000,
            //       (long)(now - previous) % 1000000);
        }
        
    }

    printf("=== END CONTROLLER ===\n");
    fflush(stdout);
}

// 吏��냽�쟻�쑝濡� 紐⑦꽣媛믪쓣 蹂대궡�뒗 �벐�젅�뱶
void run_dpram(void*) {
    RTIME now, previous;

    state_data sdata;               // state_data 援ъ“泥� (dpram_dev.h�뿉�꽌 �젙�쓽)
    control_data cdata;             // control_data 援ъ“泥� (dpram_dev.h�뿉�꽌 �젙�쓽)
    unsigned long print_cnt = 0;
	

    // Open RTDM device file
    int device = rt_dev_open(DEVICE_NAME, O_RDWR);            // Device瑜� open()
    if (device < 0) {
        printf("ERROR : can't open device %s (%s)\n", DEVICE_NAME, strerror(-device));
        exit(1);
    }

    unsigned long control_ns = 1*1000*1000;

    int ret = -1;

    // Setting current task as a periodic task
    rt_task_set_periodic(&rt_task_dpram, TM_NOW, control_ns);

    for(int i = 0; i < CONTROL_JOINT_NUM; ++i){
	    cdata.position[i] = 0.0;
	    cdata.torque[i] = 0.0;
	    cdata.velocity[i] = 0.0;
    }

    while (!is_quit()) {
        previous = rt_timer_read();

        rt_dev_ioctl(device, DPRAM_RECV_STATE_DATA, &sdata);

        // Setting global variable for state data (sdata -> g_sdata)
        set_robot_state_data(sdata);

        // Getting global variable for control data (g_cdata -> cdata)
        
	//cdata = get_robot_control_data();

	for(int i = 0; i < CONTROL_JOINT_NUM; ++i){
		cdata.position[i] += 0.1;
	}


	control_data cdata_t;             // control_data 援ъ“泥� (dpram_dev.h�뿉�꽌 �젙�쓽)
        // Sends control data through DPRAM
        rt_dev_ioctl(device, DPRAM_SEND_CONTROL_DATA, &cdata);

	for(int i = 0; i < CONTROL_JOINT_NUM; ++i){
		cdata_t.position[i] = 0.0;
		cdata_t.torque[i] = 0.0;
		cdata_t.velocity[i] = 0.0;
	}


        rt_dev_ioctl(device, DPRAM_RECV_TEST_CONTROL_DATA, &cdata_t);
	double temp = cdata_t.position[0];
	bool is_fail = false;
	for(int i = 0; i < CONTROL_JOINT_NUM; ++i){
		if(std::abs(cdata_t.position[i] - temp) > 0.001)	is_fail = true;
	}
	if(is_fail) 
	{
		printf("=========================================\n");
		printf("=========================================\n");
		printf("=========================================\n");
		for(int i = 0; i < CONTROL_JOINT_NUM; ++i){
			unsigned int temp1 = *((unsigned int*)(&(cdata_t.position[i]))+0);
			unsigned int temp2 = *((unsigned int*)(&(cdata_t.position[i]))+1);
			printf("Recv control: %.4f \n", cdata_t.position[i]);    // 옜 control_data� 옜
		}
		for(int i = 0; i < CONTROL_JOINT_NUM; ++i){
			unsigned int temp1 = *((unsigned int*)(&(cdata_t.position[i]))+0);
			unsigned int temp2 = *((unsigned int*)(&(cdata_t.position[i]))+1);
			printf("Recv control: 0x%8x 0x%8x\n", temp1, temp2);    // 옜 control_data� 옜
		}
	}
	if(false)	
	{
		for(int i = 0; i < CONTROL_JOINT_NUM; ++i){
			unsigned int temp1 = *((unsigned int*)(&(cdata_t.position[i]))+0);
			unsigned int temp2 = *((unsigned int*)(&(cdata_t.position[i]))+1);
			//printf("Recv control: 0x%8x 0x%8x\n", temp1, temp2);    // 옜 control_data� 옜
			printf("Recv control: %.4f \n", cdata_t.position[i]);    // 옜 control_data� 옜
		}
		printf("=========================================\n");
		fflush(stdout);
	}
        // Waits control time
        rt_task_wait_period(NULL);

        // Prints control time
        now = rt_timer_read();
        if(std::abs((double)(now - previous) - control_ns) > control_ns*0.1) {

            printf("DPRAM: Time since last turn: %ld.%06ld ms\n",
                    (long)(now - previous) / 1000000,
                    (long)(now - previous) % 1000000);
        }
    }


    // Closes RTDM device file
    ret = rt_dev_close(device);   // close()
    if (ret < 0) {
        printf("ERROR : can't close device %s (%s)\n", DEVICE_NAME, strerror(-ret));
        exit(1);
    }
    printf("=== END DPRAM ===\n");
    fflush(stdout);
}


template <typename T>
T getInput()
{
    T result;
    std::cin >> result;
    if (std::cin.fail() || std::cin.get() != '\n') {
        std::cin.clear();
        while (std::cin.get() != '\n');
        throw std::ios_base::failure("Invalid input.");
    }
    return result;
}



int main( ) {

    std::cout << "Start main controller code" << std::endl;

    // Prevents memory swapping
    int ret = mlockall(MCL_CURRENT | MCL_FUTURE);
    if (ret) {
        perror("ERROR : mlockall has failled");
        exit(1);
    }


    // Makes a RT task for current main thread
    ret = rt_task_shadow(&rt_task_main, NULL, 1, 0);
    if (ret)
    {
        fprintf(stderr, "ERROR : rt_task_shadow: %s\n", strerror(-ret));
        exit(1);
    }



    // Defines serial socket for IMU sensor
    boost::asio::io_service io;
    boost::asio::serial_port serial_for_imu(io);
    serial_for_imu.open("/dev/ttyO2");
    serial_for_imu.set_option(boost::asio::serial_port::baud_rate(115200UL));
    serial_for_imu.set_option(boost::asio::serial_port::flow_control());
    serial_for_imu.set_option(boost::asio::serial_port::stop_bits());
    serial_for_imu.set_option(boost::asio::serial_port::character_size());
    serial_for_imu.set_option(boost::asio::serial_port::parity());

    // Copy socket reference to global variable
    g_serial_for_imu = &serial_for_imu;

    // Starts async read function for IMU sensor 
    serial_for_imu.async_read_some(
        boost::asio::buffer(g_recv_buffer_for_imu,1024),
        handler_imu);


    // Defines stack size for RT tasks
    const unsigned int stack_size = 10*1024*1024;

    // Creates RT tasks 
    rt_task_create(&rt_task_controller, "controller", stack_size, 99, 0);
    rt_task_create(&rt_task_imu, "imu", stack_size, 99, 0);
    //rt_task_create(&rt_task_hokuyo, "hokuyo", stack_size, 99, 0);
    rt_task_create(&rt_task_dpram, "dpram", stack_size, 99, 0);


    // Starts RT tasks corresponding it's functions
    rt_task_start(&rt_task_controller, &run_controller, NULL);
    rt_task_start(&rt_task_imu, &run_imu, NULL);
    //rt_task_start(&rt_task_hokuyo, &run_hokuyo, NULL);
    rt_task_start(&rt_task_dpram, &run_dpram, NULL);

    // Starts an io thread to receive packets from the IMU sensor
    boost::thread thread_for_io(boost::bind(&boost::asio::io_service::run, &io));
    //boost::thread thread_for_dpram([&](){ run_dpram(NULL); });



    // Defines sending function to IMU sensor
    auto lambda_for_send_imu_command = [&](const std::string& data) {
        serial_for_imu.write_some(boost::asio::buffer(data.c_str(), data.size()));
    };

    while(!is_quit()) {
        std::cout << "=============  COMMAND  =============" << std::endl;
        std::cout << " q : quit" << std::endl;
        std::cout << " i : initialize IMU sensor" << std::endl;
        std::cout << std::endl;
        std::cout << "Enter your Key: ";

        char key;
        try {
            key = getInput<char>();
        } catch( std::exception& e) {
            key = 0x00;
            std::cout << "Wrong key input" << std::endl;
        }

        switch(key) {
        case 'q':
            quit();
            break;
        case 'i':
        {
            std::cout << "----- calibration acceleration sensor ----" << std::endl;
            lambda_for_send_imu_command("<cas>");
            sleep(2.0);
            std::cout << "----- calibration gyro sensor ----" << std::endl;
            lambda_for_send_imu_command("<cg>");
            sleep(2.0);
            std::cout << "----- clear pre-saved motion offset ----" << std::endl;
            lambda_for_send_imu_command("<cmco>");
            sleep(2.0);
            std::cout << "----- saving current motion offset ----" << std::endl;
            lambda_for_send_imu_command("<cmo>");
            break;
        }
	case 's':
	{
		CONTROL_START = 1;
	}
        }
    }

    sleep(1.0);

    // Waits until task being finished 
    rt_task_join(&rt_task_dpram);
    rt_task_join(&rt_task_controller);
    //rt_task_join(&rt_task_hokuyo);
    rt_task_join(&rt_task_imu);


    io.stop();
    thread_for_io.join();
    serial_for_imu.close();
    




    return 0;
}



// Setting function for quaternion variable
void set_imu_quat(const Eigen::Quaterniond& quat) {
    std::lock_guard<std::mutex> lock(g_mutex_for_imu);
    g_imu_quat = quat;
}

// Getting function for quaternion variable
Eigen::Quaterniond get_imu_quat( ) {
    std::lock_guard<std::mutex> lock(g_mutex_for_imu);
    return g_imu_quat;
}

// Setting function for scanned points variable
void set_scanned_points(const Eigen::MatrixXd& scanned_points) {
    std::lock_guard<std::mutex> lock(g_mutex_for_hokuyo);
    g_scanned_points = scanned_points;
}

// Getting function for scanned points variable
Eigen::MatrixXd get_scanned_points( ) {
    std::lock_guard<std::mutex> lock(g_mutex_for_hokuyo);
    return g_scanned_points;
}

// Setting function for robot state data variable
void set_robot_state_data(const state_data& sdata) {
    std::lock_guard<std::mutex> lock(g_mutex_for_controller);
    g_sdata = sdata;
}

// Getting function for robot state data variable
state_data get_robot_state_data( ) {
    std::lock_guard<std::mutex> lock(g_mutex_for_controller);
    return g_sdata;
}

// Setting function for robot control data variable
void set_robot_control_data(const control_data& cdata) {
    std::lock_guard<std::mutex> lock(g_mutex_for_controller);
    g_cdata = cdata;
}

// Getting function for robot control data variable
control_data get_robot_control_data( ) {
    std::lock_guard<std::mutex> lock(g_mutex_for_controller);
    return g_cdata;
}
