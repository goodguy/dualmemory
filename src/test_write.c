#include <stdio.h>
#include <string.h>        // strlen()
#include <fcntl.h>         // O_WRONLY
#include <unistd.h>        // write(), close()
#include <dpram_dev.h>
#include <time.h>

/* TEST 프로그램 
 *
 * dpram에 control_data, state_data를 쓰는 테스트 프로그램 
 *   - ioctl()함수를 통하여, dpram에 접근
 *   - 정해진 구조체에 대한, 접근이 용이
 */

int main(int argc, char* argv[])
{

    printf("HELLO!\n"); fflush(stdout);
    int fd = open("/dev/dpram", O_RDWR);           // device file open()
    int i = 0;
    int k = 0;

    int send_count = 10;            // 테스트 횟수
    state_data sdata;               // state_data 구조체 (dpram_dev.h에서 정의)
    control_data cdata;             // control_data 구조체 (dpram_dev.h에서 정의)
         


    clock_t begin = clock();
    for(k = 0; k < send_count; ++k){
        // 매 iteration 마다, dpram에 write할 값을 증가 시킴
        for(i = 0; i < CONTROL_JOINT_NUM; ++i){
            cdata.torque[i] = i*3 + 10.123123+k;
            cdata.velocity[i] = i*3 + 11.4334+k;
            cdata.position[i] = i*3 + 12.5343+k;
            sdata.torque[i] = i*3 + 13.123123+k;
            sdata.velocity[i] = i*3 + 14.4334+k;
            sdata.position[i] = i*3 + 15.5343+k;
        }

        ioctl(fd, DPRAM_SEND_CONTROL_DATA, &cdata);      // ioctl() 함수를 통하여, control_data를 씀
        ioctl(fd, DPRAM_SEND_TEST_STATE_DATA, &sdata);   // ioctl() 함수를 통하여, state_data를 씀
    }

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    
    printf("Spent time: %.4f s\t for %.4f MByte\n", time_spent, (double)send_count*sizeof(control_data)/1024.0/1024.0);

    close(fd);                    // Device close()
    printf("HELLO!\n"); fflush(stdout);

    return 0;
}
