#include <stdio.h>
#include <string.h>        // strlen()
#include <fcntl.h>         // O_WRONLY
#include <unistd.h>        // write(), close()
#include <dpram_dev.h>
#include <time.h>


/* TEST 프로그램 
 *
 * dpram에 저장된 control_data, state_data를 읽는 테스트 프로그램 
 *   - ioctl()함수를 통하여, dpram에 접근
 *   - 정해진 구조체에 대한, 접근이 용이
 */
int main(int argc, char* argv[])
{

    printf("HELLO!\n"); fflush(stdout); 
    int fd = open("/dev/dpram", O_RDWR);      // Device file open()
    int i = 0;
    int k = 0;

    // ----------- TEST  ----------------------
    state_data sdata;               // state_data 구조체 (dpram_dev.h에서 정의)
    control_data cdata;             // control_data 구조체 (dpram_dev.h에서 정의)


    int send_count = 10000;         // 테스트 횟수

    clock_t begin = clock();
    for(k = 0; k < send_count; ++k){
        ioctl(fd, DPRAM_RECV_TEST_CONTROL_DATA, &cdata);      // ioctl() 함수를 통하여, control_data를 읽음
        ioctl(fd, DPRAM_RECV_STATE_DATA, &sdata);             // ioctl() 함수를 통하여, state_data를 읽음
    }
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    double mbs = (double)send_count*(sizeof(state_data)+sizeof(control_data))/1024.0/1024.0/time_spent;
    printf("Spent time: %.4f s\t for %.4f MByte [%.4f MB/s]\n", time_spent, (double)send_count*(sizeof(state_data)+sizeof(control_data))/1024.0/1024.0, mbs);

    for(i = 0; i < CONTROL_JOINT_NUM; ++i){ 
        printf("Recv control: %.4f %.4f %.4f \n", cdata.torque[i], cdata.velocity[i], cdata.position[i]);    // 읽은 control_data를 표시
    }
    printf("=========================================\n");
    for(i = 0; i < CONTROL_JOINT_NUM; ++i){
        printf("Recv state: %.4f %.4f %.4f \n", sdata.torque[i], sdata.velocity[i], sdata.position[i]);      // 읽은 state_data를 표시 
    }

    close(fd);                    // Device close()
    printf("HELLO!\n"); fflush(stdout);

    return 0;
}
