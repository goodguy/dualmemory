
set(TOOLCHAIN_ROOT_PATH /usr/lib/arm-none-eabi/)


################ Do not touch below if you don't need fix ############
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)

# Setting cross compiler 
set(CMAKE_TOOLCHAIN_PREFIX  arm-none-eabi)
set(CMAKE_C_COMPILER    ${CMAKE_TOOLCHAIN_PREFIX}-gcc)
set(CMAKE_CXX_COMPILER  ${CMAKE_TOOLCHAIN_PREFIX}-g++)



# Setting root path of Cross compiler
set(CMAKE_FIND_ROOT_PATH  ${TOOLCHAIN_ROOT_PATH} )

# Setting OpenOCD root path to automatically write binary file
set(OPENOCD_ROOT_PATH /usr/share/openocd/)

set(CMAKE_C_FLAGS "-mlittle-endian -mthumb -mthumb-interwork -nostartfiles -mcpu=cortex-m4 -std=gnu99 -g -O2 -Wall -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -DUSE_STDPERIPH_DRIVER -DSTM32F4XX" CACHE STRING "c flags")
set(CMAKE_CXX_FLAGS ${CMAKE_C_FLAGS} CACHE STRING "c++ flags")
