cmake_minimum_required(VERSION 2.6)
project(MYCORTEX C ASM)

set(MYCORTEX_OUTPUT_LIB_DIR ${MYCORTEX_BINARY_DIR}/lib)
set(MYCORTEX_OUTPUT_BIN_DIR ${MYCORTEX_BINARY_DIR}/bin)



make_directory(${MYCORTEX_OUTPUT_LIB_DIR})
make_directory(${MYCORTEX_OUTPUT_BIN_DIR})

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${MYCORTEX_OUTPUT_LIB_DIR}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${MYCORTEX_OUTPUT_BIN_DIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${MYCORTEX_OUTPUT_LIB_DIR}")




include_directories(
    ${MYCORTEX_SOURCE_DIR}/drv/
    ${MYCORTEX_SOURCE_DIR}/lib/inc/
    ${MYCORTEX_SOURCE_DIR}/lib/inc/peripherals
    ${MYCORTEX_SOURCE_DIR}/lib/inc/core
)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Ofast")

FILE (GLOB STM32F4XX_LIB_C_SOURCES lib/src/peripherals/*.c drv/*.c)
add_library (stm32f4xxlib ${STM32F4XX_LIB_C_SOURCES})

set_source_files_properties(lib/startup_stm32f40xx.s PROPERTIES COMPILE_FLAGS "-x assembler-with-cpp")

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -T ${MYCORTEX_SOURCE_DIR}/mycortex.ld")


set(sources
    src/main.c
    src/stm32f4xx_it.c
    src/system_stm32f4xx.c
    src/syscalls.c
    src/utils.c
)

add_executable( test_stm ${sources} lib/startup_stm32f4xx.s)
#set_target_properties( test_stm PROPERTIES COMPILE_FLAGS ${COMPILE_FLAGS} "-DUSE_STDPERIPH_DRIVER -DSTM32F4XX")
target_link_libraries( test_stm stm32f4xxlib c m)


set(target_test_stm test_stm)
add_custom_command( TARGET ${target_test_stm} POST_BUILD 
    COMMAND ${CMAKE_TOOLCHAIN_PREFIX}-objcopy -O binary ${MYCORTEX_OUTPUT_BIN_DIR}/${target_test_stm} ${MYCORTEX_OUTPUT_BIN_DIR}/${target_test_stm}.bin ) 


add_custom_target( flash_test_stm 
    COMMAND sudo openocd -f interface/stlink-v2.cfg -f target/stm32f4x_stlink.cfg -c "init" -c "reset halt" -c "program ${MYCORTEX_OUTPUT_BIN_DIR}/${target_test_stm} verify reset" -c "reset run" -c shutdown
    DEPENDS ${target_test_stm} )
