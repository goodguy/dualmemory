#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "stm32f4xx_conf.h"
#include "utils.h"
#include "led.h"

#define MAX_BUF_SIZE (256)

#define DPRAM_ADDR_PERIPH           (RCC_AHB1Periph_GPIOD)                     // 주소핀에 대한 GPIO peripheral
#define DPRAM_ADDR_PORT             (GPIOD)                                    // 주소핀에 대한 GPIO port
#define DPRAM_ADDR_PINS             (0xFFF)                                    // 주소핀에 대한 GPIO pins

#define DPRAM_DATA_PERIPH           (RCC_AHB1Periph_GPIOE)                     // 데이터핀에 대한 GPIO peripheral
#define DPRAM_DATA_PORT             (GPIOE)                                    // 데이터핀에 대한 GPIO port
#define DPRAM_DATA_PINS             (0xFFFF)                                   // 데이터핀에 대한 GPIO pins

#define DPRAM_CONTROL_PERIPH        (RCC_AHB1Periph_GPIOB)                     // 제어핀에 대한 GPIO peripheral
#define DPRAM_CONTROL_PORT          (GPIOB)                                    // 제어핀에 대한 GPIO port
#define DPRAM_CONTROL_PINS          (0x1F)                                     // 제어핀에 대한 GPIO pins
#define DPRAM_OEN_PIN               (GPIO_Pin_0)                               // 제어핀(output enable)에 대한 GPIO pin
#define DPRAM_WEN_PIN               (GPIO_Pin_1)                               // 제어핀(write enable)에 대한 GPIO pin
#define DPRAM_CEN_PIN               (GPIO_Pin_2)                               // 제어핀(chip enable)에 대한 GPIO pin
#define DPRAM_BUSYN_PIN             (GPIO_Pin_3)                               // 제어핀(busy)에 대한 GPIO pin
#define DPRAM_SEMN_PIN              (GPIO_Pin_4)                               // 제어핀(semaphore enable)에 대한 GPIO pin

// 각 제어핀에 대한 컨트롤 값 정의
#define DPRAM_CEN_ENABLE            (Bit_RESET) 
#define DPRAM_CEN_DISABLE           (Bit_SET) 

#define DPRAM_OEN_ENABLE            (Bit_RESET) 
#define DPRAM_OEN_DISABLE           (Bit_SET) 

#define DPRAM_WEN_WRITE_MODE        (Bit_RESET) 
#define DPRAM_WEN_READ_MODE         (Bit_SET) 

#define DPRAM_SEMN_ENABLE           (Bit_RESET) 
#define DPRAM_SEMN_DISABLE          (Bit_SET) 


#define MAX_DPRAM_ADDR              (0xFFF)                                    // DPRAM의 최대 메모리 사이즈
#define MAX_DPRAM_SEM_ADDR          (0x7)                                      // DPRAM semaphore의 최대 메모리 사이즈

// ioctl() 함수와의 transfer test를 위한 정의
#define CONTROL_JOINT_NUM           (20)
#define CONTROL_MEM_LOC             (0x0)
#define CONTROL_DATA_SIZE           (sizeof(control_data)/2)
#define STATE_MEM_LOC               (CONTROL_MEM_LOC+CONTROL_DATA_SIZE)
#define STATE_DATA_SIZE             (sizeof(state_data)/2)

#define SEM_ADDR_CONTROL_DATA       (0x0)                                      // control_data 영역에 대한 semaphore 주소
#define SEM_ADDR_STATE_DATA         (0x1)                                      // state_data 영역에 대한 semaphore 주소

#define SEM_DATA_LOCK               (0x0)
#define SEM_DATA_UNLOCK             (0x1)


// control_data structure 정의
typedef struct
{
    double torque[CONTROL_JOINT_NUM];
    double velocity[CONTROL_JOINT_NUM];
    double position[CONTROL_JOINT_NUM];
} control_data;


// state_data structure 정의
typedef struct
{
    double torque[CONTROL_JOINT_NUM];
    double velocity[CONTROL_JOINT_NUM];
    double position[CONTROL_JOINT_NUM];
} state_data;


// DPRAM의 operation 상태를 확인하기 위함 (상태에 따라, gpio pin 설정을 하기 위함)
typedef enum 
{
    DPRAM_IDLE = 0,        // DPRAM의 초기상태
    DPRAM_NOR_WRITE,       // DPRAM이 normal write를 수행할 때
    DPRAM_NOR_READ,        // DPRAM이 normal read를 수행할 때
    DPRAM_SEM_WRITE,       // DPRAM이 semaphore write를 수행할 때
    DPRAM_SEM_READ         // DPRAM이 semaphore read를 수행할 때
} dpram_mode_t;


// 현재, 시간을 저장하기 위한 변수 (timer peripheral 이용)
volatile uint32_t time_var1, time_var2;


volatile dpram_mode_t dpram_mode = DPRAM_IDLE;   // DPRAM의 operation 상태 변수


void usleep(volatile uint32_t nCount);           // micro time마다 sleep을 하기 위한 함수
void test();                                     // ioctl() 함수를 테스트하기 위함
void test2();                                    // 전체 메모리의 read, write 성능을 확인하기 위함

// DPRAM이 busy 상태일 때 (사실 busy 상태 체크는 master, slave 형태로 DPRAM을 구성하였을 경우 필요)
uint8_t is_busy(){
    uint8_t busy_bit = GPIO_ReadInputDataBit(DPRAM_CONTROL_PORT, DPRAM_BUSYN_PIN);
    return (busy_bit == 0x0);
}

// GPIO핀을 write 모드로 구성
void set_dpram_write_mode(){

    dpram_mode = DPRAM_NOR_WRITE;                                              // operation 상태 업데이트
    GPIO_InitTypeDef  GPIO_InitStructure;

    // Set output mode for data pins
    GPIO_InitStructure.GPIO_Pin = DPRAM_DATA_PINS;                             
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(DPRAM_DATA_PORT, &GPIO_InitStructure);                           // Data pins에 대하여, output 모드로 설정 변경

    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_WEN_PIN, DPRAM_WEN_WRITE_MODE);    // write enable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_OEN_PIN, DPRAM_OEN_DISABLE);       // output disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_SEMN_PIN, DPRAM_SEMN_DISABLE);     // semaphore disable
}


// GPIO핀을 read 모드로 구성
void set_dpram_read_mode(){
    dpram_mode = DPRAM_NOR_READ;                                               // operation 상태 업데이트

    GPIO_InitTypeDef  GPIO_InitStructure;

    // Set input mode for data pins
    GPIO_InitStructure.GPIO_Pin = DPRAM_DATA_PINS;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(DPRAM_DATA_PORT, &GPIO_InitStructure);                           // Data pins에 대하여, input 모드로 설정 변경

    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_OEN_PIN, DPRAM_OEN_ENABLE);        // output enable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_WEN_PIN, DPRAM_WEN_READ_MODE);     // write disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_SEMN_PIN, DPRAM_SEMN_DISABLE);     // semaphore disable
}

// GPIO핀을 semaphore read 모드로 구성
void set_dpram_sem_read_mode(){
    dpram_mode = DPRAM_SEM_READ;                                               // operation 상태 업데이트

    GPIO_InitTypeDef  GPIO_InitStructure;

    // Set input mode for data pins
    GPIO_InitStructure.GPIO_Pin = DPRAM_DATA_PINS;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(DPRAM_DATA_PORT, &GPIO_InitStructure);                           // Data pins에 대하여, input 모드로 설정 변경

    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_WEN_PIN, DPRAM_WEN_READ_MODE);     // write disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_OEN_PIN, DPRAM_OEN_ENABLE);        // output enable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_CEN_PIN, DPRAM_CEN_DISABLE);       // chip disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_SEMN_PIN, DPRAM_SEMN_ENABLE);      // semaphore enable
}

// DPRAM semaphore 영역의 값을 읽는 함수
uint16_t read_sem_mem(uint16_t addr){
    while(is_busy());
    uint16_t data;
    set_dpram_sem_read_mode();                                                 // semaphore read 모드로 변경
    GPIO_Write(DPRAM_ADDR_PORT, addr);                                         // 읽을 semaphore 주소 기입
    data = GPIO_ReadInputData(DPRAM_DATA_PORT);                                // semaphore 값을 읽음
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_SEMN_PIN, DPRAM_SEMN_DISABLE);     // semaphore disable

    return data;
}


// DPRAM semaphore 영역에 값을 쓰는 함수
uint16_t write_sem_mem(uint16_t addr, uint16_t data){
    while(is_busy());
    uint16_t sem_data = 0xff;
    dpram_mode = DPRAM_SEM_WRITE;                                              // operation 상태 업데이트
    GPIO_InitTypeDef  GPIO_InitStructure;

    // Set output mode for data pins
    GPIO_InitStructure.GPIO_Pin = DPRAM_DATA_PINS;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(DPRAM_DATA_PORT, &GPIO_InitStructure);                           // Data pins에 대하여, output 모드로 설정 변경

    GPIO_Write(DPRAM_ADDR_PORT, addr);                                         // 쓸 semaphore 주소 기입
    GPIO_Write(DPRAM_DATA_PORT, data);                                         // semaphore 값 기입

    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_CEN_PIN, DPRAM_CEN_DISABLE);       // chip disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_SEMN_PIN, DPRAM_SEMN_ENABLE);      // semaphore enable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_WEN_PIN, DPRAM_WEN_WRITE_MODE);    // write enable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_OEN_PIN, DPRAM_OEN_ENABLE);        // output enable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_WEN_PIN, DPRAM_WEN_READ_MODE);     // write disable (이 시점에서 semaphore 값이 저장 됨)

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;                               
    GPIO_Init(DPRAM_DATA_PORT, &GPIO_InitStructure);                           // Data pins에 대하여, input 모드로 설정 변경

    sem_data = GPIO_ReadInputData(DPRAM_DATA_PORT);                            // semaphore 값을 읽음

    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_OEN_PIN, DPRAM_OEN_DISABLE);       // output disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_SEMN_PIN, DPRAM_SEMN_DISABLE);     // semaphore disable

    return sem_data;
}


// 일반데이터를 DPRAM에 쓸 경우
void write_mem(uint16_t addr, uint16_t data){
    while(is_busy());
    if(dpram_mode != DPRAM_NOR_WRITE)   set_dpram_write_mode();                // write mode로 gpio 설정 변경
    GPIO_Write(DPRAM_ADDR_PORT, addr);                                         // write 할 주소 지정
    GPIO_Write(DPRAM_DATA_PORT, data);                                         // write 할 데이터 지정
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_CEN_PIN, DPRAM_CEN_ENABLE);        // chip enable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_WEN_PIN, DPRAM_WEN_WRITE_MODE);    // write enable (실제로 write가 이루어지는 시점)
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_WEN_PIN, DPRAM_WEN_READ_MODE);     // write disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_CEN_PIN, DPRAM_CEN_DISABLE);       // chip disable
}

// 일반데이터를 DPRAM에서 읽을 경우
uint16_t read_mem(uint16_t addr){
    while(is_busy());
    if(dpram_mode != DPRAM_NOR_READ)   set_dpram_read_mode();                  // read mode로 gpio 설정 변경
    uint16_t data = 0x0000;

    GPIO_Write(DPRAM_ADDR_PORT, addr);                                         // read 할 주소 지정
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_CEN_PIN, DPRAM_CEN_ENABLE);        // chip enable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_OEN_PIN, DPRAM_OEN_ENABLE);        // output enable (DPRAM으로부터 데이터가 출력으로 나오는 시점)
    data = GPIO_ReadInputData(DPRAM_DATA_PORT);                                // GPIO 데이터를 읽어서 data에 저장
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_OEN_PIN, DPRAM_OEN_DISABLE);       // output disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_CEN_PIN, DPRAM_CEN_DISABLE);       // chip disable

    return data;
}

// semaphore lock
void lock(uint16_t addr){
    uint16_t sem_data = write_sem_mem(addr, SEM_DATA_LOCK);                    // semaphore lock 요청
    while((sem_data & 0x1) != 0x0)                                             // semaphore 가 lock이 될 때까지 기다림
    {
        sem_data = read_sem_mem(addr);                                         // semaphore 값을 읽음
        //printf("SEM: %x\n\r", sem_data);
        usleep(10);
    }
}

// semaphore unlock
void unlock(uint16_t addr){
    uint16_t sem_data = write_sem_mem(addr, SEM_DATA_UNLOCK);                  // semaphore unlock 요청
    while((sem_data & 0x1) != 0x1)                                             // semaphore가 unlock이 될 때까지 기다림
    {
        //printf("SEM: %x\n\r", sem_data);
        sem_data = read_sem_mem(addr);                                         // semaphore 값을 읽음
        usleep(10);
    }
}

// control_data를 DPRAM에 씀
void write_control_data(control_data* data){
    lock(SEM_ADDR_CONTROL_DATA);                                               // control_data영역 access 권한 획득
    uint16_t addr = CONTROL_MEM_LOC;
    uint16_t data_size = CONTROL_DATA_SIZE;
    uint16_t i;

    for(i = 0; i < data_size; ++i){
        write_mem(addr++, ((uint16_t*)data)[i]);                               // control_data write 수행
    }
    unlock(SEM_ADDR_CONTROL_DATA);                                             // control_data영역 access 권한 해제
}

// control_data를 DPRAM에서 읽음
void read_control_data(control_data* data){
    lock(SEM_ADDR_CONTROL_DATA);                                               // control_data영역 access 권한 획득
    uint16_t addr = CONTROL_MEM_LOC;
    uint16_t data_size = CONTROL_DATA_SIZE;
    uint16_t i;

    for(i = 0; i < data_size; ++i){
       ((uint16_t*)data)[i] = read_mem(addr++);                                // control_data read 수행
    }

    unlock(SEM_ADDR_CONTROL_DATA);                                             // control_data영역 access 권한 해제
}

// state_data를 DPRAM에 씀
void write_state_data(state_data* data){
    lock(SEM_ADDR_STATE_DATA);                                                 // state_data영역 access 권한 획득
    uint16_t addr = STATE_MEM_LOC;
    uint16_t data_size = STATE_DATA_SIZE;
    uint16_t i;

    for(i = 0; i < data_size; ++i){
        write_mem(addr++, ((uint16_t*)data)[i]);                               // state_data write 수행 
    }
    unlock(SEM_ADDR_STATE_DATA);                                               // state_data영역 access 권한 해제
}

// state_data를 DPRAM에서 읽음
void read_state_data(state_data* data){
    lock(SEM_ADDR_STATE_DATA);                                                 // state_data영역 access 권한 획득
    uint16_t addr = STATE_MEM_LOC;
    uint16_t data_size = STATE_DATA_SIZE;
    uint16_t i;

    for(i = 0; i < data_size; ++i){
       ((uint16_t*)data)[i] = read_mem(addr++);                                // state_data read 수행 
    }
    unlock(SEM_ADDR_STATE_DATA);                                               // state_data영역 access 권한 해제
}


// control_data를 출력하기 위한 함수
void print_control_data(control_data* data){
    uint16_t i;
    printf("\n\r==========CONTROL DATA===========\n\r");
    for(i = 0; i < CONTROL_JOINT_NUM; ++i){
        printf("%.2f \t%.2f \t%.2f \n\r", data->torque[i], data->velocity[i], data->position[i]);
    }
    printf("\n\r=================================\n\r");
}

// state_data를 출력하기 위한 함수
void print_state_data(state_data* data){
    uint16_t i;
    printf("\n\r===========STATE DATA============\n\r");
    for(i = 0; i < CONTROL_JOINT_NUM; ++i){
        printf("%.2f \t%.2f \t%.2f \n\r", data->torque[i], data->velocity[i], data->position[i]);
    }
    printf("\n\r=================================\n\r");
}

// DPRAM에 관한 초기화 루틴 (GPIO 초기화, 프로그램 변수 초기화)
void init_dpram(){
    dpram_mode = DPRAM_IDLE;
    GPIO_InitTypeDef  GPIO_InitStructure;

    // DPRAM 관련 GPIO peripherals clock enable
    // 총 3개의 GPIO peripheral 사용: GPIO_D(주소핀), GPIO_E(데이터핀), GPIO_B(제어핀)
    RCC_AHB1PeriphClockCmd(DPRAM_ADDR_PERIPH, ENABLE);         
    RCC_AHB1PeriphClockCmd(DPRAM_DATA_PERIPH, ENABLE);
    RCC_AHB1PeriphClockCmd(DPRAM_CONTROL_PERIPH, ENABLE);


    // 주소핀에 대한 GPIO 출력 설정
    GPIO_InitStructure.GPIO_Pin = DPRAM_ADDR_PINS;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(DPRAM_ADDR_PORT, &GPIO_InitStructure);

    // 데이터핀에 대한 GPIO 출력 설정 (read 할때는 입력으로 설정 변경)
    GPIO_InitStructure.GPIO_Pin = DPRAM_DATA_PINS;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(DPRAM_DATA_PORT, &GPIO_InitStructure);

    // 제어핀에 대한 GPIO 출력 설정 (output enable pin, write enable pin, chip enable pin, semaphore enable pin)
    GPIO_InitStructure.GPIO_Pin = DPRAM_OEN_PIN|DPRAM_WEN_PIN|DPRAM_CEN_PIN|DPRAM_SEMN_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(DPRAM_CONTROL_PORT, &GPIO_InitStructure);
    // 제어핀에 대한 GPIO 입력 설정 (busy pin - master, slave 모드일 때 필요)
    GPIO_InitStructure.GPIO_Pin = DPRAM_BUSYN_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(DPRAM_CONTROL_PORT, &GPIO_InitStructure);

    // 제어핀 초기값 설정
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_SEMN_PIN, DPRAM_SEMN_DISABLE);   // semaphore disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_CEN_PIN, DPRAM_CEN_DISABLE);     // chip disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_OEN_PIN, DPRAM_OEN_DISABLE);     // output disable
    GPIO_WriteBit(DPRAM_CONTROL_PORT, DPRAM_WEN_PIN, DPRAM_WEN_WRITE_MODE);  // write enable

    int i;
    for(i = 0; i < MAX_DPRAM_SEM_ADDR; ++i){
        unlock(i);    // 모든 semaphore에 대해서 unlock 수행
    }
}

// 시스템 초기화 루틴
void init() {
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    // systic timer 설정 (1us 마다 tick 발생)
    if (SysTick_Config(SystemCoreClock / 1000/ 1000)) {
        while (1) {};
    }


    // UART1과 관련 GPIO 포트에 대한 clock enable 수행 
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);


    // UART1에 관련된 GPIO 핀에 대해서, peripheral과 관련된 출력으로 핀 설정
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;             //  peripheral과 관련된 출력으로 핀 설정
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // UART1에 관련된 GPIO 핀에 대해서, UART1과 관련된 출력으로 핀 설정
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

    // UART1과 관련된 interrupt 설정 enable
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);          // RX 신호 발생시, interrupt 발생

    // UART1 통신 관련 설정
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
    USART_Init(USART1, &USART_InitStructure);

    // UART1 enable
    USART_Cmd(USART1, ENABLE);

    // DPRAM 관련 초기화 수행
    init_dpram();
}


// UART1에 입력으로 들어온 데이터를 읽음
uint8_t get_input(char* buf){
    uint8_t count = 0; 
    // UART1에 입력으로 들어온 데이터의 flags가 unset 될때까지 read 수행
    while(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == SET){
        buf[count++] = USART_ReceiveData(USART1);  // UART1에 들어온 데이터 read 수행
    }
    if(count != 0){
        buf[count] = '\0';
    }
    return count;
}

int main(void) {
    init();         // 시스템 초기화

    LED_Init();     // LED 초기화
    LED_R_ON();     
    LED_G_ON();

    test();         // ioctl() functionality 테스트
    test2();        // 전체 DPRAM의 read() & write() 성능 테스트

    for(;;) {

    }

    return 0;
}


/* TEST 프로그램 2
 *
 * bbb와 함께 전체 저장공간(0~0xfff)에 숫자를 하나씩 증가시켜가며 dpram 공유 테스트 프로그램
 *   0(mycortex)->1(bbb)->2(mycortex)->3(bbb)-> ... 
 *   - 전체 공간에 대해서, 지속적으로 read()를 하여 값이 달라진 경우에 대해서만 write() 수행
 */
void test2() {

    const size_t max_mem_size = 0x1000;
    unsigned short* data_out = malloc(sizeof(short)*max_mem_size);        // write()를 위한 저장공간  
    unsigned short* data_in = malloc(sizeof(short)*max_mem_size);         // read()를 위한 저장공간


    unsigned short val = 0x0;
    memset((void*)data_in, 0, sizeof(short)*max_mem_size);

    int loc;

    int count = 0;

    lock(SEM_ADDR_CONTROL_DATA);                     // control_data를 위한 semaphore를 활용하여, lock()
    for(loc = 0; loc < max_mem_size; ++loc){
        data_out[loc] = val;                         // 전체 DPRAM공간에 0 을 기입
        write_mem(loc, data_out[loc]);               // 전체 DPRAM공간에 0 을 기입
    }
    unlock(SEM_ADDR_CONTROL_DATA);                   // control_data를 위한 semaphore를 활용하여, unlock()

    // 테스트 시작
    for(;;) {
        usleep(5000);                                // 5 ms 대기
        lock(SEM_ADDR_CONTROL_DATA);                 // control_data를 위한 semaphore를 활용하여, lock()
        for(loc = 0; loc < max_mem_size; ++loc){ 
            data_in[loc] = read_mem(loc);            // 전체 메모리의 값을 read()
        }
        if(data_in[0] != val){                       // 기존에 mycortex에서 write() 한 값과 다를 경우, 읽은 값에서 +1을 수행
            val = data_in[0] + 1;
        }
        for(loc = 0; loc < max_mem_size; ++loc){
            data_out[loc] = val;                     // 값 업데이트
            write_mem(loc, data_out[loc]);           // 업데이트된 값을 DPRAM에 write()
        }
        unlock(SEM_ADDR_CONTROL_DATA);               // control_data를 위한 semaphore를 활용하여, unlock()

        int error = 0;
        for(loc = 0; loc < max_mem_size; ++loc){
            error += abs((int)data_in[loc] - (int)data_out[loc]);
        }

        // 쓴 값과 읽은 값이 동일(error==0)하거나, bbb에서 +1을 한 경우(error==max_mem_size)에 대해서는 무시.
        //   하지만, semaphore가 정상동작하지 않을 경우, 메모리 access가 동시(bbb & mycortex)에 이루어지므로 stable한 전송이 이루어지지 않는다. 해당 경우에 대해서 그 에러값이 표시됨.
        if((error != 0 && error != max_mem_size && count != 0)){
            printf("[%d]Error: %d, %d, %d\n\r",count, error, data_in[0], val);
        }
        else if(count % 100 == 0){
            printf("[%d]Current state: input: %d, output: %d\n\r",count, data_in[0], val);
        }

        count++;
    }

    free(data_out);
    free(data_in);
}


// TEST1에 대해서, 키보드 입력 표시
void print_test(){
    printf("1: read data\n\r");
    printf("2: write test data1\n\r");
    printf("3: write test data2\n\r");
    printf("4: lock control semaphore\n\r");
    printf("5: unlock control semaphore\n\r");
    printf("6: lock state semaphore\n\r");
    printf("7: unlock state semaphore\n\r");
    printf("q: quit and speed test\n\r");
}


/* TEST 프로그램 1
 *
 * dpram에 저장된 control_data, state_data를 읽거나 쓰는 테스트 프로그램 
 *   - ioctl()함수를 통하여, dpram에 접근
 *   - 정해진 구조체에 대한, 접근이 용이
 */

void test() {

    char input[MAX_BUF_SIZE] = {0};

    int i;

    uint16_t addr = 0x0;
    uint16_t data = 0xff;


    control_data cdata;        // control_data 구조체 정의
    state_data sdata;          // state_data 구조체 정의

    // 구조체 값 초기화
    for(i = 0; i < CONTROL_JOINT_NUM; ++i){
        cdata.torque[i] = i*3 + 0;
        cdata.velocity[i] = i*3 + 1;
        cdata.position[i] = i*3 + 2;
        sdata.torque[i] = 0;
        sdata.velocity[i] = 0;
        sdata.position[i] = 0;
    }



    data = 0x0;
    printf("DPRAM initialization started!\n\r");
    for(addr = 0; addr <= MAX_DPRAM_ADDR; ++addr){
        write_mem(addr, data);   // 모든 메모리 공간에 대해서 0을 write()
    }
    addr = 0x0;
    printf("DPRAM initialization finished!\n\r");


    print_test();                // 키보드 입력 예시 출력

    for(;;) {

        uint8_t size = get_input(input);           // 키보드 입력 쿼리

        if(size != 0){
            printf("INPUT: %s\n\r", input);

            if(input[0] == '1'){                   // DPRAM에 저장된 control_data, state_data를 읽음
                read_control_data(&cdata);         // control_data read()
                read_state_data(&sdata);           // state_data read()
                print_control_data(&cdata);        // 출력
                print_state_data(&sdata);          // 출력
            }
            else if(input[0] == '2'){              // DPRAM에 임의의 control_data 기입 1
                for(i = 0; i < CONTROL_JOINT_NUM; ++i){
                    cdata.torque[i] = i*3 + 0x7f;
                    cdata.velocity[i] = i*3 + 0x8f;
                    cdata.position[i] = i*3 + 0x9f;
                }
                write_control_data(&cdata);        // control_data write()
            }
            else if(input[0] == '3'){              // DPRAM에 임의의 control_data 기입 2
                for(i = 0; i < CONTROL_JOINT_NUM; ++i){
                    cdata.torque[i] = 0.12345;
                    cdata.velocity[i] = 1.11111;
                    cdata.position[i] = 2.22222;
                }
                write_control_data(&cdata);        // control_data write()
            }
            else if(input[0] == '4'){              // control_data 영역 lock() test
                lock(SEM_ADDR_CONTROL_DATA);
                printf("SEM ADDR[%x] is locked!\n\r", SEM_ADDR_CONTROL_DATA);
            }
            else if(input[0] == '5'){              // control_data 영역 unlock() test
                unlock(SEM_ADDR_CONTROL_DATA);
                printf("SEM ADDR[%x] is unlocked!\n\r", SEM_ADDR_CONTROL_DATA);
            }
            else if(input[0] == '6'){              // state_data 영역 lock() test
                lock(SEM_ADDR_STATE_DATA);
                printf("SEM ADDR[%x] is locked!\n\r", SEM_ADDR_STATE_DATA);
            }
            else if(input[0] == '7'){              // state_data 영역 unlock() test
                unlock(SEM_ADDR_STATE_DATA);
                printf("SEM ADDR[%x] is unlocked!\n\r", SEM_ADDR_STATE_DATA);
            }
            else if(input[0] == 'q'){              // 현재 테스트를 종료하고, test2()로 넘어감.
                printf("================= QUIT!=========================\n\r");
                break;
            }
            print_test();

        }
    }
}


// Systick 발생시 호출됨 (단위: 1us)
void timing_handler() {
    if (time_var1) {       // usleep() 에서 셋팅될 경우
        time_var1--;
    }
    time_var2++;           // 전체 시간 
}


// UART1에서 입력 발생시 호출됨 (별다른 작업하지 않음)
void usart1_handler() {
    LED_G_OFF();           // green LED를 끔
}


// micro second 단위로 쉬는 함수
void usleep(volatile uint32_t nCount) {
    time_var1 = nCount;
    while(time_var1) {};
}


void _init() {
    // Nothing to do
}

