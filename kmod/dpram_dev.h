#ifndef __DPRAM_DEV_H__
#define __DPRAM_DEV_H__

#include <linux/ioctl.h>


#define DPRAM_ID_NUM (100)       // ioctl 명령어의 unique한 번호를 할당해주기 위한 ID number 정의
#define CONTROL_JOINT_NUM (20)   // ioctl 명령어를 통해 제어할 조인트 갯수 
#define DPRAM_MAX_SIZE (0x1000)  // DPRAM의 최대 사이즈 (16bit x 1k)


// BBB -> myCortex 로 전송할 데이터 구조
typedef struct
{
    double torque[CONTROL_JOINT_NUM];
    double velocity[CONTROL_JOINT_NUM];
    double position[CONTROL_JOINT_NUM];
} control_data;

// myCortex -> BBB 로 수신할 데이터 구조
typedef struct
{
    double torque[CONTROL_JOINT_NUM];
    double velocity[CONTROL_JOINT_NUM];
    double position[CONTROL_JOINT_NUM];
} state_data;

// ioctl instructions 정의
#define DPRAM_SEND_CONTROL_DATA         _IOW( DPRAM_ID_NUM, 0, control_data )
#define DPRAM_RECV_STATE_DATA           _IOR( DPRAM_ID_NUM, 1, state_data )

// 테스트용 ioctl instructions 정의
#define DPRAM_RECV_TEST_CONTROL_DATA    _IOR( DPRAM_ID_NUM, 2, control_data )
#define DPRAM_SEND_TEST_STATE_DATA      _IOW( DPRAM_ID_NUM, 3, state_data )

#endif
