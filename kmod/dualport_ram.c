#include <linux/init.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/ioctl.h>
#include <linux/time.h>
#include <asm/uaccess.h>   /* copy_to_user */
#include <linux/cdev.h>
#include <linux/sched.h>
#include <linux/memory.h>
#include <linux/dma-mapping.h>
#include <linux/edma.h>
#include <linux/platform_data/edma.h>
#include <linux/delay.h>
#include <linux/mutex.h>
#include <linux/gpio.h>

//device tree support
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/of_dma.h>
#include <linux/of_gpio.h>
#include <linux/completion.h>

#include <rtdm/rtdm_driver.h>


#include "dpram_dev.h"          // ioctl instructions 정의에 관한 헤더파일


#define DEVICE_NAME "dpram"     // 생성할 디바이스 파일 이름 정의

#define SEM_DATA_LOCK   (0x0)   // semaphore에 0을 기입시 lock
#define SEM_DATA_UNLOCK (0x1)   // semaphore에 1을 기입시 unlock


#define MAX_DMA_TRANSFER_IN_BYTES   (DPRAM_MAX_SIZE*2)      // DMA로 전송할 최대 데이터량 == DPRAM 저장용량



/******* DPRAM ADDRESS USAGE (normal) *******
 *                                          *
 * 0x0 ------------------------------------ *
 *             control_data area            *
 * ---------------------------------------- *
 *               state_data area            *
 * ---------------------------------------- *
 *                  empty                   *
 * 0xfff ---------------------------------- *
 *                                          *
 ********************************************/

/****** DPRAM ADDRESS USAGE (semaphore) *****
 *                                          *
 * 0x0 ------------------------------------ *
 *             control_data semaphore       *
 * 0x1 ------------------------------------ *
 *               state_data semaphore       *
 * 0x2 ------------------------------------ *
 *                  empty                   *
 * 0x7 ------------------------------------ *
 *                                          *
 ********************************************/



// 디바이스 파일 operation functions 사전 정의
static int     dpram_open(struct inode *inode, struct file *filp);
static int     dpram_release(struct inode *inode, struct file *filp);
static loff_t  dpram_lseek(struct file *file, loff_t offset, int orig);
static ssize_t dpram_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos);
static ssize_t dpram_read(struct file *filp, char *buf, size_t count, loff_t *f_pos);
static long    dpram_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);

// DPRAM의 semaphore 저장공간(주소: 0x0~0x7)에 읽기/쓰기를 위한 함수 정의
static unsigned short dpram_read_sem(unsigned int offset); 
static unsigned short dpram_write_sem(unsigned int offset, unsigned short data);


// DMA를 이용한 mem2mem (DPRAM <-> DMA) 복사를 위한 함수 정의
static int     edma_memtomemcpy(int count, unsigned long src_addr, unsigned long trgt_addr, int dma_ch);

// DMA를 통한 memory copy가 완료 또는 에러가 날 경우 호출되는 함수
static void    dma_callback(unsigned dma_ch, u16 ch_status, void *data);


// 디바이스파일에 관한 operation functions 정의
static struct file_operations dpram_ops = {
    .llseek = dpram_lseek,              // lseek() 호출시 수행
    .read =   dpram_read,               // read() 호출시 수행
    .write =  dpram_write,              // write() 호출시 수행
    .compat_ioctl =  dpram_ioctl,       // ioctl() 호출시 수행
    .unlocked_ioctl = dpram_ioctl,      // ioctl() 호출시 수행
    .open =   dpram_open,               // open() 호출시 수행
    .release =  dpram_release,          // close() 호출시 수행
};



// For DMA transfer
static dma_addr_t          dma_buf_addr = 0;           // DMA buffer 주소를 담는 변수
static unsigned char*      dma_buf = NULL;             // DMA buffer
static int                 dma_channel = 0;            // 할당된 DMA channel 번호를 담는 변수
static volatile int        dma_callback_status = 0;    // DMA 전송 종료시, return 되는 status
static struct completion   dma_comp;                   // DMA 전송이 끝났는지를 알려주는 변수


// For character device
static unsigned char       dpram_major_num = 0;        // Character device를 구분할 major 번호
static struct cdev         dpram_cdev;                 // Character device 구조체


// For device tree
static struct class*       dpram_class;                // 디바이스 트리에서 사용되는 디바이스의 class 구조체 (/sys/class)
static struct device*      dpram_device;               // 디바이스 트리에서 사용되는 디바이스 구조체


// For DPRAM specification
static const unsigned char*   dpram_base_addr = (unsigned char *)0x08000000;   // DPRAM의 물리적주소를 담는 변수 (DTS파일의 reg 변수를 통해 사전 정의됨)
static unsigned char*         dpram_virt_addr = 0;                             // DPRAM의 가상주소를 담는 변수
static const unsigned long    dpram_size = DPRAM_MAX_SIZE;                     // DPRAM의 전체 사이즈
static const unsigned int     dpram_sem_offset_control_data = 0;               // DPRAM의 control_data 접근을 위한 semaphore 주소
static const unsigned int     dpram_sem_offset_state_data = 1;                 // DPRAM의 state_data 접근을 위한 semaphore 주소

// For driver operation
static int    is_opened = 0;       // 디바이스파일이 사전에 열려져 있는지 알려주는 변수
static int    write_count = 0;     // 디버깅용 write 횟수 체크 변수


// For GPIO 
static unsigned int       dpram_gpio_pin_size = 8;                                                          // 디버깅용 LED GPIO pin size
static unsigned int       dpram_gpio_pin_numbers[] = {15, 117, 14, 115, 113, 111, 112, 110};                // 디버깅용 LED GPIO pin number
static bool               dpram_gpio_turn_on[] = {false, false, false, false, false, false, false, false};  // 디버깅용 LED values

static unsigned int       dpram_gpio_sem_pin_number = 31;    // DPRAM의 semaphore 저장공간 접근을 위한 gpio 핀 번호
static unsigned int       dpram_gpio_cen_pin_number = 48;    // DPRAM의 chip enable을 위한 gpio 핀 번호




// semaphore lock (offset = semaphore 주소)
void lock(const unsigned int offset){
    uint16_t sem_data;

    // semaphore lock 요청
    sem_data = dpram_write_sem(offset, SEM_DATA_LOCK);
    while((sem_data & 0x1) != 0x0)
    {
        sem_data = dpram_read_sem(offset);     // semaphore가 lock이 됐는지 읽음
        udelay(10);                            // 10us sleep
    } 
}

// semaphore unlock (offset = semaphore 주소)
void unlock(const unsigned int offset){
    uint16_t sem_data;

    // semaphore unlock 요청
    sem_data = dpram_write_sem(offset, SEM_DATA_UNLOCK);
    while((sem_data & 0x1 ) != 0x1)
    {
        sem_data = dpram_read_sem(offset);     // semaphore가 unlock이 됐는지 읽음
        udelay(10);                            // 10us sleep
    }
}



// 디버깅용 LED를 통한 8bits visuallization 함수
static void gpio_display(unsigned char val){
    int i = 0;
    for(i = 0; i < dpram_gpio_pin_size; ++i){
        dpram_gpio_turn_on[i] = (((val>>i)&0x1)&&0x1);
        gpio_set_value(dpram_gpio_pin_numbers[i], dpram_gpio_turn_on[i]);     // GPIO set value        
    }
}

// DPRAM semaphore 저장공간의 데이터를 읽는 함수 
static unsigned short dpram_read_sem(unsigned int offset){
    unsigned short sem_val = 0x0;

    gpio_set_value(dpram_gpio_cen_pin_number, true);                 // Chip access disable (false: disable, true: enable)
    gpio_set_value(dpram_gpio_sem_pin_number, false);                // Semaphore access enable (false: disable, true: enable)
    sem_val = (unsigned short)ioread16(dpram_virt_addr+offset*2);    // Directly read data for semaphore
    gpio_set_value(dpram_gpio_sem_pin_number, true);                 // semaphore access disable (false: disable, true: enable)

    return sem_val;  // return semaphore value
}

// DPRAM semaphore 저장공간의 데이터를 쓰는 함수 
static unsigned short dpram_write_sem(unsigned int offset, unsigned short data){
    unsigned short sem_val = 0x0;

    gpio_set_value(dpram_gpio_cen_pin_number, true);                 // Chip access disable (false: disable, true: enable)
    gpio_set_value(dpram_gpio_sem_pin_number, false);                // Semaphore access enable (false: disable, true: enable)
    iowrite16(data, dpram_virt_addr+offset*2);                       // Directly write data for semaphore
    gpio_set_value(dpram_gpio_sem_pin_number, true);                 // Semaphore access disable (false: disable, true: enable)
    gpio_set_value(dpram_gpio_sem_pin_number, false);                // Semaphore access re-enable for read operation (false: disable, true: enable)
    sem_val = (unsigned short)ioread16(dpram_virt_addr+offset*2);    // Directly read data for semaphore
    gpio_set_value(dpram_gpio_sem_pin_number, true);                 // semaphore access disable (false: disable, true: enable)

    return sem_val;  // return semaphore value
}



// DPRAM 일반 저장공간의 데이터를 쓰는 함수 
static ssize_t dpram_write_impl(const unsigned char* buf, size_t count, size_t offset){
    unsigned short int transfer_size;
    ssize_t transferred = 0;

    // DMA -> DPRAM memcpy
    unsigned long src_addr = (unsigned long)dma_buf_addr;
    unsigned long trgt_addr = (unsigned long)&dpram_base_addr[offset];

    // 한번에 보낼 수 있는 데이터 사이즈가 DMA buffer 사이즈를 초과할 수 있으므로, 이를 방지하기 위한 코드
    if(count < MAX_DMA_TRANSFER_IN_BYTES){
        transfer_size = count;                       // 초과하지 않을 경우, 전송할 사이즈로 정의
    } 
    else {
        transfer_size = MAX_DMA_TRANSFER_IN_BYTES;   // 초과할 경우, 전송 최대 사이즈로 정의
    }

    if(dma_buf == NULL) {
        printk("%s: failed to allocate DMA buffer \n", DEVICE_NAME);
        return -1;
    }

    // 전송할 user 메모리 공간의 데이터를 RTDM의 메모리 공간인 DMA buffer로 복사
    if(copy_from_user(dma_buf, buf, transfer_size)) {
        return -1;
    }

    // DPRAM chip access enable (false: disable, true: enable)
    gpio_set_value(dpram_gpio_cen_pin_number, false);

    // 전송 시작
    while (transferred < count) {

        // DMA 모듈을 통하여, DMA buffer -> DPRAM 으로 복사
        if(edma_memtomemcpy(transfer_size, src_addr, trgt_addr, dma_channel) < 0) {
            printk("%s: write: Failed to trigger EDMA transfer.\n", DEVICE_NAME);
            return -1;
        }

        trgt_addr += transfer_size;      // 남은 데이터 전송(최대 전송사이즈를 초과할 경우)을 위한 주소 셋팅
        transferred += transfer_size;    // 전송한 데이터량

        // 남은 데이터 전송(최대 전송사이즈를 초과할 경우)을 위한 다음 전송량 설정
        if ((count - transferred) < MAX_DMA_TRANSFER_IN_BYTES) {
            transfer_size = count - transferred;
        } else {
            transfer_size = MAX_DMA_TRANSFER_IN_BYTES;
        }

        // 남은 user 메모리 공간의 전송할 데이터를 RTDM의 메모리 공간인 DMA buffer로 복사
        if (copy_from_user(dma_buf, &buf[transferred], transfer_size)) {
            return -1;
        }
    }

    // DPRAM chip access disable (false: disable, true: enable)
    gpio_set_value(dpram_gpio_cen_pin_number, true);

    // 디버깅용 LED setting
    gpio_display((unsigned char)++write_count);

    return transferred;
}

// DPRAM 일반 저장공간의 데이터를 읽는 함수 
static ssize_t dpram_read_impl(unsigned char* buf, size_t count, size_t offset){
    unsigned short int transfer_size;
    ssize_t transferred = 0;

    // DPRAM -> DMA memcpy
    unsigned long src_addr = (unsigned long)&dpram_base_addr[offset];
    unsigned long trgt_addr = (unsigned long)dma_buf_addr;


    // 한번에 수신할 수 있는 데이터 사이즈가 DMA buffer 사이즈를 초과할 수 있으므로, 이를 방지하기 위한 코드
    if(count < MAX_DMA_TRANSFER_IN_BYTES){
        transfer_size = count;
    } 
    else {
        transfer_size = MAX_DMA_TRANSFER_IN_BYTES;
    }

    if(dma_buf == NULL) {
        printk("%s: failed to allocate DMA buffer \n", DEVICE_NAME);
        return -1;
    }

    // DPRAM chip access enable (false: disable, true: enable)
    gpio_set_value(dpram_gpio_cen_pin_number, false);

    // 수신 시작
    while (transferred < count) {

        // DMA 모듈을 통하여, DPRAM -> DMA buffer 로 복사
        if(edma_memtomemcpy(transfer_size, src_addr, trgt_addr, dma_channel) < 0) {
            printk("%s: read: Failed to trigger EDMA transfer.\n", DEVICE_NAME);
            return -1;
        }

        // 수신받은 DMA 버퍼의 데이터를 user 메모리 공간으로 복사
        if (copy_to_user(&buf[transferred], dma_buf, transfer_size)) {
            return -1;
        }

        src_addr += transfer_size;      // 남은 데이터 수신(최대 전송사이즈를 초과할 경우)을 위한 주소 셋팅
        transferred += transfer_size;   // 수신한 데이터량

        // 남은 데이터 수신(최대 전송사이즈를 초과할 경우)을 위한 다음 수신량 설정
        if ((count - transferred) < MAX_DMA_TRANSFER_IN_BYTES) {
            transfer_size = count - transferred;
        } else {
            transfer_size = MAX_DMA_TRANSFER_IN_BYTES;
        }

    }
    // DPRAM chip access disable (false: disable, true: enable)
    gpio_set_value(dpram_gpio_cen_pin_number, true);

    // 디버깅용 LED setting
    gpio_display((unsigned char)--write_count);

    return transferred;
}

// ioctl() - control_data를 전송하기 위한 함수
static int send_control_data(const control_data* control_data_val){
    int transferred;
    lock(dpram_sem_offset_control_data);    // lock semaphore to access data area
    // Write control data
    transferred = (int)dpram_write_impl((const unsigned char*)control_data_val, sizeof(control_data), 0);
    unlock(dpram_sem_offset_control_data);  // unlock semaphore to access data area
    return transferred;
}

// ioctl() - control_data를 수신하기 위한 함수
static int recv_control_data(control_data* control_data_val){
    int transferred;
    lock(dpram_sem_offset_control_data);    // lock semaphore to access data area
    // Read control data
    transferred = (int)dpram_read_impl((unsigned char*)control_data_val, sizeof(control_data), 0);
    unlock(dpram_sem_offset_control_data);  // unlock semaphore to access data area
    return transferred;
}

// ioctl() - state_data를 전송하기 위한 함수
static int send_state_data(const state_data* state_data_val){
    int transferred;
    lock(dpram_sem_offset_state_data);      // lock semaphore to access data area
    // Write state data (control_data 영역 다음에 접근)
    transferred = (int)dpram_write_impl((const unsigned char*)state_data_val, sizeof(state_data), sizeof(control_data));
    unlock(dpram_sem_offset_state_data);    // unlock semaphore to access data area
    return transferred;
}

// ioctl() - state_data를 수신하기 위한 함수
static int recv_state_data(state_data* state_data_val){
    int transferred;
    lock(dpram_sem_offset_state_data);      // lock semaphore to access data area
    // Read state data (control_data 영역 다음에 접근)
    transferred = (int)dpram_read_impl((unsigned char*)state_data_val, sizeof(state_data), sizeof(control_data));
    unlock(dpram_sem_offset_state_data);    // unlock semaphore to access data area
    return transferred;
}

// lseek() - cursor 위치를 옮기기 위한 함수
static loff_t dpram_lseek(struct file *file, loff_t offset, int orig){
    loff_t new_pos = 0;
    switch(orig) {
        case 0 : /*seek set*/
            new_pos = offset;
            break;
        case 1 : /*seek cur*/
            new_pos = file->f_pos + offset;
            break;
        case 2 : /*seek end*/
            new_pos = dpram_size - offset;
            break;
    }
    if(new_pos > dpram_size)
        new_pos = dpram_size;
    if(new_pos < 0)
        new_pos = 0;
    file->f_pos = new_pos;
    return new_pos;
}


// write() - 디바이스파일에 데이터를 쓸 경우 호출 (control_data를 위한 semaphore를 사용)
static ssize_t dpram_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos){
    ssize_t transferred;
    lock(dpram_sem_offset_control_data);    // lock semaphore to access data area
    // write 수행
    transferred = dpram_write_impl((const unsigned char*)buf, count, (size_t)(*f_pos));
    unlock(dpram_sem_offset_control_data);  // unlock semaphore to access data area
    return transferred;
}

// read() - 디바이스파일을 읽을 경우 호출 (control_data를 위한 semaphore를 사용)
static ssize_t dpram_read(struct file *filp, char *buf, size_t count, loff_t *f_pos){
    ssize_t transferred;
    lock(dpram_sem_offset_control_data);    // lock semaphore to access data area
    // read 수행
    transferred = dpram_read_impl((unsigned char*)buf, count, (size_t)(*f_pos));
    unlock(dpram_sem_offset_control_data);  // unlock semaphore to access data area
    return transferred;
}

// ioctl() - 디바이스파일에 ioctl 함수를 쓸 경우 호출
static long dpram_ioctl(struct file *filp, unsigned int cmd, unsigned long arg){

    // dpram_dev.h 에 정의된 instructions에 관하여 switch 문으로 구분
    switch( cmd ){
        case DPRAM_SEND_CONTROL_DATA:           // control_data를 전송시
            {
                send_control_data((const control_data*)arg);
                return 0;
            }
        case DPRAM_RECV_STATE_DATA:             // state_data를 수신시
            {
                recv_state_data((state_data*)arg);
                return 0;
            }
        case DPRAM_RECV_TEST_CONTROL_DATA:      // control_data를 수신시
            {
                recv_control_data((control_data*)arg);
                return 0;
            }
        case DPRAM_SEND_TEST_STATE_DATA:        // state_data를 전송시
            {
                send_state_data((const state_data*)arg);
                return 0;
            }
    }
    return -ENOTTY;
}

// DMA를 통한 memory copy 함수 (DPRAM <-> DMA buffer)
static int edma_memtomemcpy(int count, unsigned long src_addr, unsigned long trgt_addr, int dma_ch){
    int result = 0;
    struct edmacc_param param_set;

    edma_set_src(dma_ch, src_addr, INCR, W256BIT);        // source 설정
    edma_set_dest(dma_ch, trgt_addr, INCR, W256BIT);      // destination 설정
    edma_set_src_index(dma_ch, 1, 1);
    edma_set_dest_index(dma_ch, 1, 1);
    edma_set_transfer_params(dma_ch, count, 1, 1, 1, ASYNC); // one block of one frame of one array of count bytes

    /* Enable the Interrupts on Channel 1 */
    edma_read_slot(dma_ch, &param_set);   // 기존에 설정된 DMA 설정파라미터 로드
    param_set.opt |= ITCINTEN;        // Interrupt enable (intermediate)
    param_set.opt |= TCINTEN;         // Completion interrupt enable
    param_set.opt |= EDMA_TCC(EDMA_CHAN_SLOT(dma_ch));    // 할당된 DMA channel를 사용
    edma_write_slot(dma_ch, &param_set);  // DMA 설정파라미터 업데이트
    dma_callback_status = 0u;         // DMA callback status 초기화
    dma_comp.done = 0;                // DMA completion 초기화
    result = edma_start(dma_ch);      // DMA 전송 시작 (종료 또는 에러 발생시 interrupt 발생 -> dma_callback() 호출)

    if (result != 0) {
        rtdm_printk("%s: edma copy failed \n", DEVICE_NAME);
    }

    wait_for_completion(&dma_comp);   // DMA 전송이 끝날때까지 대기 

    /* Check the status of the completed transfer */
    if (dma_callback_status < 0) {
        rtdm_printk("%s: edma copy: Event Miss Occured!!!\n", DEVICE_NAME);
        edma_stop(dma_ch);            // 에러가 났을 경우, DMA 전송 취소
        result = -EAGAIN;
    }

    return result;
}

// DMA를 통한 memory copy가 완료 또는 에러가 날 경우 호출되는 함수
static void dma_callback(unsigned dma_ch, u16 ch_status, void *data){
    switch (ch_status) {
        case DMA_COMPLETE:   // 완료가 되었을 때
            dma_callback_status = 1;
            break;

        case DMA_CC_ERROR:   // 에러가 나타났을 때
            dma_callback_status = -1;
            break;

        default:             // 그 외 모든 경우
            dma_callback_status = -1;
            break;
    }

    complete(&dma_comp);     // dma_comp를 done으로 설정해주어, wait_for_completion() 함수에서 넘어가도록 해줌
}

// open() - 디바이스파일을 열었을 경우에 호출
static int dpram_open(struct inode *inode, struct file *filp){

    // 이미 열렸을 경우에는 추가적인 설정을 안함
    if(is_opened == 1){
        printk("%s: module already opened\n", DEVICE_NAME);
        return 0;
    }

    // 물리적 주소에 대한 접근을 커널에 요청
    request_mem_region(((unsigned long)dpram_base_addr), dpram_size, DEVICE_NAME);

    // 접근을 위한 가상 주소 할당 요청
    dpram_virt_addr = ioremap_nocache(((unsigned long)dpram_base_addr), dpram_size);

    // DMA 모듈을 통한 데이터 전송을 위해, 빈 channel을 획득 및 callback 함수 정의
    dma_channel = edma_alloc_channel(EDMA_CHANNEL_ANY, dma_callback, NULL, EVENTQ_0);

    // DMA buffer 공간 확보
    dma_buf = (unsigned char *) dma_alloc_coherent(NULL, MAX_DMA_TRANSFER_IN_BYTES, &dma_buf_addr, 0);
    printk("%s: EDMA channel %d reserved \n", DEVICE_NAME, dma_channel);

    if(dma_channel < 0){
        printk("%s: edma_alloc_channel failed for dma_ch, error: %d\n", DEVICE_NAME, dma_channel);
        return -1;
    }

    printk("%s: DPRAM interface opened \n", DEVICE_NAME);

    // 이미 열렸음을 표시
    is_opened = 1;

    return 0;
}

// close() - 디바이스파일을 닫았을 경우에 호출
static int dpram_release(struct inode *inode, struct file *filp){
    
    // 이미 닫혔을 경우에는 추가적인 설정을 안함
    if(is_opened != 1){
        printk("%s: module already released\n", DEVICE_NAME);
        return 0;
    }

    // 할당된 DMA buffer 공간 해제
    dma_free_coherent(NULL, MAX_DMA_TRANSFER_IN_BYTES, dma_buf, dma_buf_addr);

    // 할당된 DMA channel 해제
    edma_free_channel(dma_channel);

    // 할당된 가상주소 해제
    iounmap(dpram_virt_addr);

    // 물리적 주소에 대한 접근을 해제
    release_mem_region(((unsigned long)dpram_base_addr), dpram_size);

    printk("%s: Release: module released\n", DEVICE_NAME);
    is_opened = 0;

    return 0;
}

// insmod dualport_ram.ko 를 하였을 경우에 호출
static int __init dpram_init(void){
    int result;
    int devno;
    int i;

    dev_t dev = 0;

    result = alloc_chrdev_region(&dev, 0, 1, DEVICE_NAME);                // character device 영역 할당

    dpram_major_num = MAJOR(dev);                                         // 할당된 character device의 할당된 major 번호 쿼리

    if (result < 0) {
        printk(KERN_ALERT "Registering char device failed with %d\n", dpram_major_num);
        return result;
    }

    devno = MKDEV(dpram_major_num, 0);                                    // device number 생성

    dpram_class = class_create(THIS_MODULE, DEVICE_NAME);                 // 디바이스트리에 새롭게 class 생성
    device_create(dpram_class, dpram_device, devno, NULL, DEVICE_NAME);   // 디바이스트리에 생성한 class에 디바이스 등록


    cdev_init(&dpram_cdev, &dpram_ops);                                   // character device structure 생성 (파일 operations 정보 포함)
    cdev_add(&dpram_cdev, devno, 1);                                      // 할당된 character device에 cdev structure 연결 및 device file 생성

    // DMA의 작업이 끝남을 알려주는 변수 초기화
    init_completion(&dma_comp);
    printk(KERN_INFO "%s: device driver is initialized\n", DEVICE_NAME);


    // 디버깅용 LED의 핀번호가 valid한지 체크
    for(i = 0; i < dpram_gpio_pin_size; ++i){
        if (!gpio_is_valid(dpram_gpio_pin_numbers[i])){
            printk(KERN_INFO "%s: gpio: invalid LED GPIO\n", DEVICE_NAME);
            return -ENODEV;
        }
    }

    // 디버깅용 LED의 GPIO 설정
    for(i = 0; i < dpram_gpio_pin_size; ++i){
        dpram_gpio_turn_on[i] = true;
        gpio_request(dpram_gpio_pin_numbers[i], "sysfs");          // 해당 GPIO 핀의 사용을 요청
        gpio_direction_output(dpram_gpio_pin_numbers[i], dpram_gpio_turn_on[i]);   // 해당 GPIO 핀의 입출력 설정
        gpio_export(dpram_gpio_pin_numbers[i], false);             // user 프로그램에서 바로 access할 수 있도록 export 함 (/sys/class/gpio/)
    }

    // DPRAM의 semaphore enable을 위한 gpio 설정
    gpio_request(dpram_gpio_sem_pin_number, "sysfs");          // 해당 GPIO 핀의 사용을 요청
    gpio_direction_output(dpram_gpio_sem_pin_number, true);    // 해당 GPIO 핀의 입출력 설정
    gpio_export(dpram_gpio_sem_pin_number, false);             // user 프로그램에서 바로 access할 수 있도록 export 함 (/sys/class/gpio/)

    // DPRAM의 chip enable을 위한 gpio 설정
    gpio_request(dpram_gpio_cen_pin_number, "sysfs");          // 해당 GPIO 핀의 사용을 요청
    gpio_direction_output(dpram_gpio_cen_pin_number, true);    // 해당 GPIO 핀의 입출력 설정
    gpio_export(dpram_gpio_cen_pin_number, false);             // user 프로그램에서 바로 access할 수 있도록 export 함 (/sys/class/gpio/)

    return 0;
}

// rmmod dualport_ram.ko 를 하였을 경우에 호출
static void __exit dpram_exit(void){
    dev_t devno = MKDEV(dpram_major_num, 0);              // device number 생성
    int i = 0;

    cdev_del(&dpram_cdev);                                // character device file 삭제
    device_destroy(dpram_class, devno);                   // 디바이스트리에 등록된 디바이스 해제
    class_destroy(dpram_class);                           // 디바이스트리에 새롭게 생성한 class 해제
    unregister_chrdev_region(devno, 1);                   // 할당된 character device 영역 해제

    for(i = 0; i < dpram_gpio_pin_size; ++i){
        gpio_set_value(dpram_gpio_pin_numbers[i], 0);
        gpio_unexport(dpram_gpio_pin_numbers[i]);         // user 프로그램에서 바로 access할 수 없도록 unexport 함 (/sys/class/gpio/)
        gpio_free(dpram_gpio_pin_numbers[i]);             // 해당 GPIO 핀의 사용을 해제
    }
    gpio_set_value(dpram_gpio_sem_pin_number, true);
    gpio_unexport(dpram_gpio_sem_pin_number);             // user 프로그램에서 바로 access할 수 없도록 unexport 함 (/sys/class/gpio/)
    gpio_free(dpram_gpio_sem_pin_number);                 // 해당 GPIO 핀의 사용을 해제

    gpio_set_value(dpram_gpio_cen_pin_number, true);
    gpio_unexport(dpram_gpio_cen_pin_number);             // user 프로그램에서 바로 access할 수 없도록 unexport 함 (/sys/class/gpio/)
    gpio_free(dpram_gpio_cen_pin_number);                 // 해당 GPIO 핀의 사용을 해제

    printk(KERN_INFO "%s: device driver is removed\n", DEVICE_NAME);
} 

// DTS 파일의 설정을 읽어 올 수 있도록 compatible 설정
static const struct of_device_id dpram_of_match[] = {
    { .compatible = DEVICE_NAME, },
    { },
};


MODULE_DEVICE_TABLE(of, dpram_of_match);   // compatible과 일치하는 로드된 DTS 파일 내용을 of에 매핑
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Deok-Hwa Kim <realsanai123@gmail.com>");

module_init(dpram_init);    // init 함수 설정
module_exit(dpram_exit);    // exit 함수 설정

