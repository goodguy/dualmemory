cmake_minimum_required(VERSION 2.6)
project(DUALMEMORY)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${DUALMEMORY_SOURCE_DIR}/cmake/")

set(DUALMEMORY_OUTPUT_LIB_DIR ${DUALMEMORY_BINARY_DIR}/lib)
set(DUALMEMORY_OUTPUT_BIN_DIR ${DUALMEMORY_BINARY_DIR}/bin)

make_directory(${DUALMEMORY_OUTPUT_LIB_DIR})
make_directory(${DUALMEMORY_OUTPUT_BIN_DIR})

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${DUALMEMORY_OUTPUT_LIB_DIR}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${DUALMEMORY_OUTPUT_BIN_DIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${DUALMEMORY_OUTPUT_LIB_DIR}")


set(EIGEN_ROOT "${DUALMEMORY_SOURCE_DIR}/../library")
set(EIGEN_ROOT_DIR "${DUALMEMORY_SOURCE_DIR}/../library")
set(BOOST_ROOT "${DUALMEMORY_SOURCE_DIR}/../library")

find_package(Flexiport)
find_package(HokuyoAIST)
FIND_PACKAGE( Eigen3 )
FIND_PACKAGE( Eigen3 )
FIND_PACKAGE( Boost 1.52.0 COMPONENTS system regex timer thread )

set(BOARD_IP  "192.168.8.2" CACHE STRING "IP address for BBB to copy binary")
set(KERNEL_DIR "${DUALMEMORY_SOURCE_DIR}/../beagle-kernel/" CACHE STRING "Linux kernel source location")



include_directories(
    ${DUALMEMORY_SOURCE_DIR}/src 
    ${DUALMEMORY_SOURCE_DIR}/kmod 
    ${DUALMEMORY_SOURCE_DIR}/../xenomai/include 
)
link_directories(
    ${DUALMEMORY_SOURCE_DIR}/../xenomai/lib 
)

add_executable ( burn_cpu src/burn_cpu.cpp )
add_executable ( test_all src/test.c )
add_executable ( test_write src/test_write.c )
add_executable ( test_read src/test_read.c )
add_executable ( rtdm_test src/rtdm_test.c )
add_executable ( rtdm_test_write src/rtdm_test_write.c )
add_executable ( rtdm_test_read src/rtdm_test_read.c )
target_link_libraries( rtdm_test        rtdm  uitron vrtx xenomai vxworks analogy psos native pthread_rt pthread)
target_link_libraries( rtdm_test_write  rtdm  uitron vrtx xenomai vxworks analogy psos native pthread_rt pthread)
target_link_libraries( rtdm_test_read   rtdm  uitron vrtx xenomai vxworks analogy psos native pthread_rt pthread)


if(Boost_FOUND )
    MESSAGE( STATUS "Boost is found ...")
    if(EIGEN3_FOUND )
        MESSAGE( STATUS "Eigen is found ...")
        include_directories(
            ${Boost_INCLUDE_DIR} 
            ${EIGEN3_INCLUDE_DIR} 
            ${Flexiport_INCLUDE_DIRS}
            ${HokuyoAIST_INCLUDE_DIRS}
        )

        set(CMAKE_CXX_FLAGS " ${CMAKE_CXX_FLAGS}  -std=c++11 -O3 -g")

        add_executable ( controller src/controller.cpp )
        target_link_libraries( controller 
                ${Boost_LIBRARIES} 
                #${Flexiport_LIBRARIES} 
                ${HokuyoAIST_LIBRARIES}
                rtdm  uitron vrtx vxworks analogy psos native xenomai pthread_rt pthread)
        #set_target_properties( controller PROPERTIES COMPILE_FLAGS "${COMPILE_FLAGS} -g")
        add_custom_target( copy_controller 
            COMMAND scp  ${DUALMEMORY_OUTPUT_BIN_DIR}/controller root@${BOARD_IP}:/root
            DEPENDS controller)
    endif()
endif()

set(test_programs
    burn_cpu
    test_all
    test_write
    test_read
    rtdm_test
    rtdm_test_write
    rtdm_test_read
)


add_custom_target( modules COMMAND make -C ${KERNEL_DIR} M=${DUALMEMORY_SOURCE_DIR}/kmod/ ARCH=arm -j4 CROSS_COMPILE=arm-linux-gnueabihf- modules)
add_custom_target( clean_modules COMMAND make -C ${KERNEL_DIR} M=${DUALMEMORY_SOURCE_DIR}/kmod/ ARCH=arm -j4 CROSS_COMPILE=arm-linux-gnueabihf- clean)

add_custom_target( copy 
    COMMAND scp  ${DUALMEMORY_SOURCE_DIR}/kmod/*.ko ${DUALMEMORY_OUTPUT_BIN_DIR}/* root@${BOARD_IP}:/root
    DEPENDS ${test_programs} modules )
